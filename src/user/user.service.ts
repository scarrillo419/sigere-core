import { BadRequestException, Injectable, Inject, NotFoundException, UnauthorizedException, forwardRef } from '@nestjs/common';
import { Types as MongooseTypes, PaginateModel, PaginateOptions, PaginateResult, Model } from 'mongoose';
import { isEmpty } from 'lodash';
import { unlinkSync } from 'fs';
import * as gm from 'gm';
import { generate } from 'generate-password';
import * as jwt from 'jsonwebtoken';
import { generate as generateRandString } from 'randomstring';
import { AuthConstants } from '../common/constants/auth.constants';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { UpdateUserSelfDto } from './dto/update-user-self.dto';
import { User } from './interfaces/user.interface';
import { DatabaseConstants } from '../common/constants/database.constants';
import { publicPath } from '../common/constants/path.constants';
import { SignUpDto } from '../auth/dto/sign-up.dto';
import { UserStatusEnum } from './enums/user-status.enum';
import { AccessToken } from '../token/interfaces/access-token.interface';
import { UpdatePasswordUpDto } from './dto/update-password.dto';
import { UserGateway } from './user.gateway';
import { TaxService } from '../tax/tax.service';
import { UserLanguageEnum } from './enums/user-language.enum';
import { SettingService } from '../setting/setting.service';
import { UserInfo } from './interfaces/user-info.interface';
import { Setting } from '../setting/interfaces/setting.interface';
import { UpdateLanguageDto } from './dto/update-user-language.dto';
import { newUserMail } from '../common/mailjet/user';
import { TokenTypeEnum } from '../token/enums/token-type.enum';

// tslint:disable-next-line:no-var-requires
const encryptor = require('simple-encryptor')(DatabaseConstants.encryptor.key);

@Injectable()
export class UserService {

  private jwtPrivateKey: String;
  private jwtPublicKey: String;

  constructor(
    @Inject('UserModelToken') public readonly userModel: PaginateModel<User>,
    @Inject(forwardRef(() => TaxService)) public readonly taxService: TaxService,
    @Inject(forwardRef(() => SettingService)) public readonly settingService: SettingService,
    private readonly gateway: UserGateway
  ) {
    try {
      this.jwtPrivateKey = AuthConstants.cert.privateKey;
      this.jwtPublicKey = AuthConstants.cert.publicKey;
    } catch (err) {
      throw new Error('One or more certificates could not be loaded');
    }
  }

  public async findAll(request: any, userId: string): Promise<PaginateResult<User>> {
    const { sortField, query, sortOrder } = request;
    const options: PaginateOptions = {
      limit: query.limit ? parseInt(query.limit) : 10,
      page: query.page ? parseInt(query.page) : 1,
      sort: sortField ? { [sortField]: sortOrder } : { createdAt: -1 },
      populate: [
        {
          path: 'roles',
          select: [
            '_id',
            'name',
          ],
        },
      ],
    };

    const searchQuery: any = query.search ? JSON.parse(query.search) : {};

    if (searchQuery.email) searchQuery.email = { $regex: searchQuery.email, $options: 'i' };
    if (searchQuery.name) searchQuery.name = { $regex: searchQuery.name, $options: 'i' };
    if (searchQuery.createdAt) searchQuery.createdAt = { $gte: searchQuery.createdAt };
    if (searchQuery.dateLastLogin) searchQuery.dateLastLogin = { $gte: searchQuery.dateLastLogin };

    if (!searchQuery.self) searchQuery._id = { $nin: [userId] };
    delete searchQuery.self;

    console.log('userId: ', userId);
    searchQuery._id = { $ne: userId };

    return this.userModel.paginate({
      ...searchQuery,
    }, options);
  }

  public async findOne(criteria: object, projection?: string): Promise<Model<User>> {
    if (!criteria) { throw new BadRequestException(); }
    console.log('(findOne) criteria: ', criteria);
    const user = await this.userModel.findOne(criteria, projection).exec();
    if (!user) { throw new NotFoundException('user.error.notFound'); }

    return user;
  }

  public async findOneInfoUser(userid: MongooseTypes.ObjectId): Promise<Model<User>> {
    const dbUser = await this.userModel.findOne({ _id: userid })
      .populate([
        {
          path: 'roles',
        },
      ]).exec();
    const dbsetting = await this.settingService.settingModel
      .findOne({})
      .populate([
        {
          path: 'salesAndPurchases.saleTax',
          populate: { path: 'taxes' }
        },
        {
          path: 'salesAndPurchases.purchaseTax',
          populate: { path: 'taxes' }
        },
        {
          path: 'salesAndPurchases.salesAccount',
          populate: { path: 'accountingAccounts' }
        },
      ]).exec();

    const returnValue: UserInfo = {
      user: dbUser,
      settings: dbsetting,
    };
    return returnValue;
  }

  public async findOneByEmail(email: string): Promise<Model<User>> {
    const dbUser = await this.userModel.findOne({ email });
    return dbUser;
  }

  public async findOneByEmailWithPassword(email: string): Promise<Model<User>> {
    return await this.userModel.findOne({ email }, '+password')
      .populate([
        {
          path: 'roles',
        },
      ]).exec();
  }

  public async findOneById(userid: MongooseTypes.ObjectId): Promise<Model<User>> {
    return await this.findOne({ _id: userid });
  }

  public async validateCredentials(user: User, password: string): Promise<Model<User>> {
    return encryptor.decrypt(user.password) === password;
  }

  public async create(createUserDto: CreateUserDto, file: any, req: any): Promise<Model<User>> {
    if (!isEmpty(file)) {
      await new Promise((resolve, reject) => {
        gm(`${publicPath.users}/${file.filename}`)
          .crop(createUserDto.crooperData.width, createUserDto.crooperData.height, createUserDto.crooperData.x, createUserDto.crooperData.y)
          .write(`${publicPath.users}/${file.filename}`, (err) => {
            if (err) {
              reject(err);
            } else resolve();
          });
      });
    }
    if (file) createUserDto.photo = file.filename;

    const password = generate({
      length: 10,
      numbers: true,
    });
    // const randString = generateRandString({ length: 5 });

    const defaultLang = req.header('Language') || UserLanguageEnum.en;
    const createdUser = new this.userModel({
      ...createUserDto,
      password,
      language: UserLanguageEnum[defaultLang],
      // status: UserStatusEnum.pendingConfig,
      // confirmation: randString,
      status: UserStatusEnum.pendingEmail,
    });

    const dbUser = await createdUser.save();

    const accessToken = this.createAccessTokenFromUserEmail(dbUser, TokenTypeEnum.email, '24h');

    newUserMail(createdUser, password, accessToken);

    return dbUser;
  }

  public async update(userid: MongooseTypes.ObjectId, updateUserDto: UpdateUserDto, file: any): Promise<Model<User>> {
    const dbUser = await this.findOneById(userid) as PaginateModel<User>;
    if (!dbUser) { throw new BadRequestException(); }
    if (!isEmpty(file)) {
      if (dbUser.photo) {
        try {
          await unlinkSync(`${publicPath.users}/${dbUser.photo}`);
        } catch (e) {
          console.log('file no exist');
        }

      }
      await new Promise((resolve, reject) => {
        gm(`${publicPath.users}/${file.filename}`)
          .crop(updateUserDto.crooperData.width, updateUserDto.crooperData.height, updateUserDto.crooperData.x, updateUserDto.crooperData.y)
          .write(`${publicPath.users}/${file.filename}`, (err) => {
            if (err) {
              reject(err);
            } else resolve();
          });
      });
      updateUserDto.photo = file.filename;
    }
    dbUser.set(updateUserDto);
    dbUser.save();
    this.gateway.server.emit('check-user');
    return this.findOneInfoUser(userid);
  }

  public async updateSelf(email: string, updateUserSelfDto: UpdateUserSelfDto, file: any): Promise<Model<User>> {
    const dbUser = await this.findOneByEmail(email);
    if (!dbUser) { throw new BadRequestException({ message: 'updateSelf.error.userNotFound' }); }

    if (!isEmpty(file)) {
      if (dbUser.photo) {
        try {
          await unlinkSync(`${publicPath.users}/${dbUser.photo}`);
        } catch (e) {
          console.log('file no exist');
        }
      }
      await new Promise((resolve, reject) => {
        gm(`${publicPath.users}/${file.filename}`)
          .crop(updateUserSelfDto.crooperData.width, updateUserSelfDto.crooperData.height,
            updateUserSelfDto.crooperData.x, updateUserSelfDto.crooperData.y)
          .write(`${publicPath.users}/${file.filename}`, (err) => {
            if (err) {
              reject(err);
            } else resolve();
          });
      });
      updateUserSelfDto.photo = file.filename;
    }

    if (!updateUserSelfDto.photo) delete updateUserSelfDto.photo;

    dbUser.set(updateUserSelfDto);
    return dbUser.save();
  }

  public async delete(userid: MongooseTypes.ObjectId): Promise<User> {
    const dbUser = await this.findOneById(userid);
    if (!dbUser) { throw new BadRequestException(); }
    return dbUser.remove();
  }

  public async signUp(signUpDto: SignUpDto, req: any): Promise<User> {
    const defaultLang = req.header('Language') || UserLanguageEnum.en;
    const createdUser = new this.userModel({
      ...signUpDto,
      status: UserStatusEnum.pendingEmail,
      language: UserLanguageEnum[defaultLang],

    });
    return createdUser.save();
  }

  public async changePassword(email: string, updatePasswordUpDto: UpdatePasswordUpDto): Promise<Model<User>> {
    const dbUser = await this.findOneByEmailWithPassword(email);
    const passwordsMatch = await this.validateCredentials(dbUser, updatePasswordUpDto.currentPassword);
    if (!passwordsMatch) { throw new UnauthorizedException({ message: 'security.error.invalidPassword' }); }
    dbUser.set(updatePasswordUpDto);
    return dbUser.save();
  }

  async checkSelf(userid: MongooseTypes.ObjectId): Promise<Model<User>> {
    const userData = await this.findOneInfoUser(userid);
    if (userData.user.status === UserStatusEnum.inactive || userData.user.status === UserStatusEnum.deleted) {
      throw new UnauthorizedException({ message: 'verify.error.inactiveUser' });
    }
    else return userData;
  }

  public async companySettings(company = null): Promise<Model<Setting>> {
    const dbSetting = await this.settingService.settingModel
      .findOne({});
    return dbSetting;
  }

  public async updateLanguage(email: string, updateLanguageDto: UpdateLanguageDto): Promise<Model<User>> {
    const dbUser = await this.findOneByEmail(email);
    if (!dbUser) { throw new BadRequestException({ message: 'updateSelf.error.userNotFound' }); }
    dbUser.set(updateLanguageDto);
    return dbUser.save();
  }

  private createAccessTokenFromUserEmail(user: User, tokenType: any, expiresIn: string): string {
    const payload: AccessToken = {
      uid: user._id,
      type: tokenType,
      sub: user.email,
      rs: user.roles,
    };
    console.log(payload);
    const timeExpires = expiresIn;
    return jwt.sign(payload, this.jwtPrivateKey, { expiresIn: timeExpires });
  }
}
