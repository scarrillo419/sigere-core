import { Module, forwardRef } from '@nestjs/common';
import { DatabaseModule } from '../common/database/database.module';
import { AuthModule } from '../auth/auth.module';
import { UserController } from './user.controller';
import { userProviders } from './user.providers';
import { UserService } from './user.service';
import { UserGateway } from './user.gateway';
import { TaxModule } from '../tax/tax.module';
import { SettingModule } from '../setting/setting.module';

@Module({
  imports: [DatabaseModule,
     forwardRef(() => AuthModule) ,
     forwardRef(() => TaxModule),
     forwardRef(() => SettingModule)],
  controllers: [UserController],
  providers: [UserService, ...userProviders, UserGateway],
  exports: [UserService]
})
export class UserModule { }
