import { Connection } from 'mongoose';
import * as mongoosePaginate from 'mongoose-paginate';
import { UserSchema } from './schemas/user.schema';

UserSchema.plugin(mongoosePaginate);

export const userProviders = [
  {
    provide: 'UserModelToken',
    useFactory: (connection: Connection) => connection.model('User', UserSchema),
    inject: ['DbConnectionToken'],
  },
];
