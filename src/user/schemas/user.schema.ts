import { Schema } from 'mongoose';
import { DatabaseConstants } from '../../common/constants/database.constants';
import { UserStatusEnumAsArray } from '../enums/user-status.enum';
import { UserLanguageEnumAsArray } from '../enums/user-language.enum';

// tslint:disable-next-line:no-var-requires
const encryptor = require('simple-encryptor')(DatabaseConstants.encryptor.key);

export const UserSchema = new Schema({
  name: { type: String },
  lastName: { type: String },
  photo: { type: String },
  email: { type: String, unique: true, required: true },
  status: { type: String, required: true, enum: UserStatusEnumAsArray },
  password: { type: String, select: false, required: true },
  roles: [{ type: Schema.Types.ObjectId, ref: 'Role'}],
  dateLastLogin: { type: Date },
  confirmation: { type: Number },
  language: { type: String, enum: UserLanguageEnumAsArray },
}, {
  timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' },
  // do not return certain fields when saving the document
  toObject: {
    transform: (doc, ret, options) => {
      // delete ret.password;
      return ret;
    },
    virtuals: true
  },
  toJSON: {
    transform: (doc, ret, options) => {
      delete ret.password;
      delete ret.__v;
      return ret;
    },
    virtuals: true,
  },
});

// when saving the password field, hash it again
UserSchema.pre('save', async function () {
  const password = this.get('password');
  const email = this.get('email');
  if (password && this.isModified('password')) {
    const hash = encryptor.encrypt(password);
    this.set('password', hash);
  }

  if (email && this.isModified('email')) {
    this.set('email', email.toLowerCase());
  }
});