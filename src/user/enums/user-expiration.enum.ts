export enum ExpirationSettingsEnum {

  withoutcaducity = 'withoutcaducity',
  expiresTheSameDay = 'expiresTheSameDay',
  expiration15Days = 'expiration15Days',
  expiration20Days = 'expiration20Days',
  expiration30Days = 'expiration30Days',
  expiration45Days = 'expiration30Days',
  expiration60Days = 'expiration60Days',
  expiration90Days = 'expiration90Days',
  expiration120Days = 'expiration120Days',
  expiration180Days = 'expiration180Days',

}

export const ExpirationSettingsEnumAsArray = Object.keys(ExpirationSettingsEnum);