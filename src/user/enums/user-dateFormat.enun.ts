export enum DateFormatEnum {
  d1 = 'd1',
  d2 = 'd2',
  d3 = 'd3'
}
export const DateFormatEnumAsArray = Object.keys(DateFormatEnum);

