export enum DocumentsEnum {
  pdf = 'pdf',
  word = 'word',
  none = 'none',
}

export const DocumentsEnumAsArray = Object.keys(DocumentsEnum);
