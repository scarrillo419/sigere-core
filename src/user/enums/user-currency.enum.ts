export enum CurrencyEnum {
  eur = 'eur',
  usd = 'usd',
}
export const CurrencyEnumAsArray = Object.keys(CurrencyEnum);
