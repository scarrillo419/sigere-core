export enum RoleOptionsEnum {
  view = 'view',
  create = 'create',
  edit = 'edit',
  delete = 'delete',
  approve = 'approve',
  profitMargin = 'profitMargin',
}
export const RoleOptionsEnumAsArray = Object.keys(RoleOptionsEnum);
