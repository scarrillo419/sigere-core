export enum DecimalsEnum {
  v1 = 'v1',
  v2 = 'v2',
  v3 = 'v3',
  v4 = 'v4',
  v5 = 'v5',
}
export const DecimalsEnumAsArray = Object.keys(DecimalsEnum);

