export enum NumericalFormatEnum {
  v1 = 'v1',
  v2 = 'v2',
  v3 = 'v3',
  v4 = 'v4',
}
export const NumericalFormatEnumAsArray = Object.keys(NumericalFormatEnum);