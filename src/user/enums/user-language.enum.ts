export enum UserLanguageEnum {
  es = 'es',
  en = 'en',
  fr = 'fr',
}

export const UserLanguageEnumAsArray = Object.keys(UserLanguageEnum);
