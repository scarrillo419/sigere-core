import {
    SubscribeMessage,
    WebSocketGateway,
    WebSocketServer,
  } from '@nestjs/websockets';
  import { Client, Server } from 'socket.io';
  
  @WebSocketGateway()
  export class UserGateway {
    @WebSocketServer()
    server: Server;
  
    @SubscribeMessage('user-up')
    findAll(client: Client, data: any): any {
      console.log('user-up');
      return 'user-up';
    }
  }