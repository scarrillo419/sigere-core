import {
  Body, Controller, Delete, Get, Param, Post, Put,
  UseFilters, UseGuards, UseInterceptors, UploadedFile, Req
} from '@nestjs/common';
import { PaginateResult } from 'mongoose';
import { ApiTags, ApiBearerAuth, ApiConsumes } from '@nestjs/swagger';
import { diskStorage } from 'multer';
import { extname } from 'path';
import { FileInterceptor } from '@nestjs/platform-express';
import { Token } from '../common/decorators/token.decorator';
import { TokenRequirements } from '../common/decorators/token-requirements.decorator';
import { HttpExceptionFilter } from '../common/filters/http-exception.filter';
import { MongoExceptionFilter } from '../common/filters/mongo-exception.filter';
import { AccessToken } from '../token/interfaces/access-token.interface';
import { UserService } from './user.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { UpdateUserSelfDto } from './dto/update-user-self.dto';
import { User } from './interfaces/user.interface';
import { TokenTypeEnum } from '../token/enums/token-type.enum';
import { TokenGuard } from '../common/guards/token.guard';
import { AuthService } from '../auth/auth.service';
import { UpdatePasswordUpDto } from './dto/update-password.dto';
import { UpdateLanguageDto } from './dto/update-user-language.dto';
import { ModulesKeyEnum } from '../user/enums/modules-key.enum';
import { RoleOptionsEnum } from '../user/enums/role-options.enum';

@ApiTags('User')
@ApiBearerAuth()
@Controller('api/user')
@UseFilters(MongoExceptionFilter, HttpExceptionFilter)
@UseGuards(TokenGuard)
export class UserController {
  constructor(
    private readonly userService: UserService,
    private readonly authService: AuthService
  ) { }

  /**
   * Routes for authenticated users only
   */

  @Get('self')
  @TokenRequirements([TokenTypeEnum.system, TokenTypeEnum.client])
  public async getSelf(@Token() token: AccessToken): Promise<User> {
    return this.userService.findOneInfoUser(token.uid);
  }

  @Put('self')
  @UseInterceptors(FileInterceptor('file', {
    storage: diskStorage({
      destination: './public/users/',
      filename: (req, file, cb) => {
        // Generating a 32 random chars long string
        const randomName = Array(32).fill(null).map(() => (Math.round(Math.random() * 16)).toString(16)).join('');
        // Calling the callback passing the random name generated with the original extension name
        cb(null, `${randomName}${extname(file.originalname)}`);
      },
      limits: { fileSize: 2 * 1024 * 1024 },
    }),
  }))
  @TokenRequirements([TokenTypeEnum.system, TokenTypeEnum.client])
  public async updateSelf(@Token() token: AccessToken, @Body() updateUserSelfDto: UpdateUserSelfDto, @UploadedFile() file): Promise<User> {
    return this.userService.updateSelf(token.sub, updateUserSelfDto, file);
  }

  /**
   * Routes for administrators only
   */

  @Get()
  @TokenRequirements([TokenTypeEnum.system, TokenTypeEnum.client])
  public async getAll(@Req() request, @Token() token: AccessToken): Promise<PaginateResult<User>> {
    console.log('token: ', token);
    return this.userService.findAll(request, token.uid);
  }

  @Post()
  @UseInterceptors(FileInterceptor('file', {
    storage: diskStorage({
      destination: './public/users/',
      filename: (req, file, cb) => {
        // Generating a 32 random chars long string
        const randomName = Array(32).fill(null).map(() => (Math.round(Math.random() * 16)).toString(16)).join('');
        // Calling the callback passing the random name generated with the original extension name
        cb(null, `${randomName}${extname(file.originalname)}`);
      },
      limits: { fileSize: 2 * 1024 * 1024 },
    }),
  }))
  @TokenRequirements([TokenTypeEnum.system, TokenTypeEnum.client])
  public async create(@Body() createUserDto: CreateUserDto, @UploadedFile() file, @Req() request): Promise<User> {
    return this.userService.create(createUserDto, file, request);
  }

  @Put(':userid')
  @UseInterceptors(FileInterceptor('file', {
    storage: diskStorage({
      destination: './public/users/',
      filename: (req, file, cb) => {
        // Generating a 32 random chars long string
        const randomName = Array(32).fill(null).map(() => (Math.round(Math.random() * 16)).toString(16)).join('');
        // Calling the callback passing the random name generated with the original extension name
        cb(null, `${randomName}${extname(file.originalname)}`);
      },
      limits: { fileSize: 2 * 1024 * 1024 },
    }),
  }))
  @ApiConsumes('multipart/form-data')
  @TokenRequirements([TokenTypeEnum.system, TokenTypeEnum.client])
  public async update(@Param('userid') userid: string,
    @Body() updateUserDto: UpdateUserDto, @UploadedFile() file): Promise<User> {
    return this.userService.update(userid, updateUserDto, file);
  }

  @Delete(':userid')
  @TokenRequirements([TokenTypeEnum.system, TokenTypeEnum.client])
  public async delete(@Param('userid') userid: string): Promise<User> {
    return this.userService.delete(userid);
  }

  @Post('refresh-access-token')
  @TokenRequirements([TokenTypeEnum.system, TokenTypeEnum.client])
  async refreshAccessToken(@Token() token: AccessToken) {
    return await this.authService.refreshAccessToken(token);
  }

  @Put('self/password')
  @TokenRequirements([TokenTypeEnum.system, TokenTypeEnum.client])
  public async changePassword(@Token() token: AccessToken, @Body() updatePasswordUpDto: UpdatePasswordUpDto): Promise<User> {
    return this.userService.changePassword(token.sub, updatePasswordUpDto);
  }

  @Get('self/check')
  @TokenRequirements([TokenTypeEnum.system, TokenTypeEnum.client])
  public async checkSelf(@Token() token: AccessToken): Promise<User> {
    return this.userService.checkSelf(token.uid);
  }

  @Put('self/language')
  @TokenRequirements([TokenTypeEnum.system, TokenTypeEnum.client],
    [ModulesKeyEnum.user],
    [RoleOptionsEnum.edit])
  public async updateLanguage(@Token() token: AccessToken, @Body() updateLanguageDto: UpdateLanguageDto): Promise<User> {
    return this.userService.updateLanguage(token.sub, updateLanguageDto);
  }

}
