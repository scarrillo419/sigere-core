import { IsString, MinLength, IsNotEmpty, Validate } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { PasswordsNotMatch } from '../../common/validators/equalsTo';

export class UpdatePasswordUpDto {

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  @MinLength(8, {
    message: 'security.error.passwordToShort'
  })
  readonly currentPassword: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  @MinLength(8, {
    message: 'security.error.passwordToShort'
  })
  readonly password: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  @MinLength(8, {
    message: 'security.error.passwordToShort'
  })
  @Validate(PasswordsNotMatch, {
    message: 'security.error.passwordsNotMatch'
  })
  readonly confirmPassword: string;
}