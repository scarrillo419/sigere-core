import { IsEmail, IsEnum, IsOptional, IsString, MaxLength, IsArray } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { UserStatusEnum, UserStatusEnumAsArray } from '../enums/user-status.enum';
import { CrooperDto } from './cropper-dto';

export class UpdateUserDto {
  @ApiProperty()
  @IsString()
  readonly name: string;

  @ApiProperty()
  @IsOptional()
  @IsString()
  readonly lastName: string;

  @ApiProperty()
  @IsEmail()  
  readonly email: string;

  @ApiProperty()
  @IsOptional()  
  public photo: string;

  @ApiProperty()
  @Type(() => CrooperDto)
  @IsOptional()
  readonly crooperData: CrooperDto;

  @ApiProperty()
  @IsOptional()  
  readonly file: any;

  @ApiProperty()
  @IsOptional()
  public password: string;
  
  @ApiProperty({ enum: UserStatusEnumAsArray })
  @IsEnum(UserStatusEnum, { each: true })  
  readonly status: UserStatusEnum;

  @ApiProperty() 
  @IsString()
  @IsOptional()
  readonly dateLastLogin: Date;

  @ApiProperty()
  @IsOptional()
  @IsArray()
  readonly roles: any;
  
}
