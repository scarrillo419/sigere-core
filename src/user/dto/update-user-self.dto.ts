import { IsOptional, IsString, ValidateNested, IsEnum } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { CrooperDto } from './cropper-dto';
import { UserLanguageEnumAsArray, UserLanguageEnum } from '../enums/user-language.enum';

export class UpdateUserSelfDto {
  @ApiProperty()
  @IsString()
  readonly name: string;

  @ApiProperty()
  @IsOptional()
  @IsString()
  readonly lastName: string;

  @ApiProperty()
  @IsOptional()
  public photo: string;

  @ApiProperty()
  @Type(() => CrooperDto)
  @IsOptional()
  readonly crooperData: CrooperDto;

  @ApiProperty()
  @IsOptional()  
  readonly file: any;

  @ApiProperty({ enum: UserLanguageEnumAsArray })
  @IsOptional()
  @IsEnum(UserLanguageEnum, { each: true })
  readonly language: UserLanguageEnum;

  
}
