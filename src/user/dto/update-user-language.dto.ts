import { IsEnum } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

import { UserLanguageEnumAsArray, UserLanguageEnum } from '../enums/user-language.enum';

export class UpdateLanguageDto {

  @ApiProperty({ enum: UserLanguageEnumAsArray })
  @IsEnum(UserLanguageEnum, { each: true })
  readonly language: UserLanguageEnum;
  
}