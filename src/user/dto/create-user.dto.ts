import { IsEmail, IsEnum, IsOptional, IsString, Validate, IsArray, MaxLength } from 'class-validator';
import { Type } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';
import { UserStatusEnum, UserStatusEnumAsArray } from '../enums/user-status.enum';
import { NoExist } from '../../common/database/database.validator';
import { CrooperDto } from './cropper-dto';


export class CreateUserDto {
  @ApiProperty()
  @IsString()
  readonly name: string;

  @ApiProperty()
  @IsOptional()
  @IsString()
  readonly lastName: string;

  @ApiProperty()
  @IsEmail()
  @Validate(NoExist, ['User'], {
    message: 'register.error.userExists'
  })
  readonly email: string;  

  @ApiProperty()
  @IsOptional()  
  public photo: string;

  @ApiProperty()
  @Type(() => CrooperDto)
  @IsOptional()
  readonly crooperData: CrooperDto;

  @ApiProperty()
  @IsOptional()  
  readonly file: any;

  @ApiProperty({ enum: UserStatusEnumAsArray })
  @IsEnum(UserStatusEnum, { each: true })  
  readonly status: UserStatusEnum;
 
  @ApiProperty() 
  @IsString()
  @IsOptional()
  readonly dateLastLogin: Date;

  @ApiProperty()
  @IsOptional()
  @IsArray()
  readonly roles: any;
}
