export interface CrooperData {
    x: number;
    y: number;
    width: number;
    height: number;
}