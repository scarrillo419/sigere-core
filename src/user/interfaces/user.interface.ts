import { Document } from 'mongoose';
import { UserStatusEnum } from '../enums/user-status.enum';
import { UserLanguageEnum } from '../enums/user-language.enum';
import { CurrencyEnum } from '../enums/user-currency.enum';
import { DecimalsEnum } from '../enums/user-decimals.enum';
import { DocumentsEnum } from '../enums/user-documents.enum';
import { NumericalFormatEnum } from '../enums/user-numericalFormat.enum';
import { DateFormatEnum } from '../enums/user-dateFormat.enun';
import { ExpirationSettingsEnum } from '../enums/user-expiration.enum';
import { Setting } from '../../setting/interfaces/setting.interface';

export interface Account extends Document {
  nameCorpor: string;
  numIdentif: string;
  email: string;
  phone: number;
  address: string;
  population: number;
  zipCode: number;
  province: string;
  country: string;
}

export interface Theme extends Document {
  readonly navTheme: string;
  readonly primaryColor: string;
  readonly secundaryColor: string;
  readonly textColor: string;
  readonly textSecundaryColor: string;
  readonly sideBarColor: string;
  readonly headerColor: string;
  readonly photoCorporative: string;
}

export interface SalesAndPurchanse extends Document {
  readonly expiration: ExpirationSettingsEnum;
  readonly saleTax: string;
  readonly purchaseTax: string;
  salesAccount: string;
  puchanseAccount: string;
  readonly paymentColletion: string;
}
export interface Preference extends Document {
  readonly currency: CurrencyEnum;
  readonly decimals: DecimalsEnum;
  readonly languageDocuments: UserLanguageEnum;
  readonly documents: DocumentsEnum;
  readonly numericalFormat: NumericalFormatEnum;
  readonly dateFormat: DateFormatEnum;
  readonly timeZone: string;
  // readonly salesAndPurchases: SalesAndPurchanse;
}

// export interface Settings extends Document {
//   readonly language: UserLanguageEnum;
//   readonly theme: Theme;
//   readonly account: Account;
//   readonly preference: Preference;
// }

export interface User extends Document {
  readonly _id: string;
  readonly name: string;
  readonly lastName: string;
  readonly userLanguage: UserLanguageEnum;
  readonly email: string;  
  readonly photo: string;
  readonly status: UserStatusEnum;  
  readonly roles: string[];
  readonly password: string; 
  readonly language: UserLanguageEnum;
  readonly created_at: Date;
}
