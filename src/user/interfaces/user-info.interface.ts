import { User } from './user.interface';
import { Setting } from '../../setting/interfaces/setting.interface';

export interface UserInfo {
  readonly user: User;
  readonly settings: Setting;
}
