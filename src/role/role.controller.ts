import { Body, Controller, Delete, Get, Param, Post, Put, UseFilters, UseGuards, Req } from '@nestjs/common';
import { PaginateResult } from 'mongoose';
import { ApiTags, ApiBearerAuth } from '@nestjs/swagger';

import { TokenRequirements } from '../common/decorators/token-requirements.decorator';
import { HttpExceptionFilter } from '../common/filters/http-exception.filter';
import { MongoExceptionFilter } from '../common/filters/mongo-exception.filter';
import { RoleService } from './role.service';
import { CreateRoleDto } from './dto/create-role.dto';
import { Role } from './interfaces/role.interface';
import { TokenTypeEnum } from '../token/enums/token-type.enum';
import { TokenGuard } from '../common/guards/token.guard';
import { UpdateRoleDto } from './dto/update-role.dto';
import { AccessToken } from '../token/interfaces/access-token.interface';
import { Token } from '../common/decorators/token.decorator';
import { ModulesKeyEnum } from '../user/enums/modules-key.enum';
import { RoleOptionsEnum } from '../user/enums/role-options.enum';

@ApiTags('Role')
@ApiBearerAuth()
@Controller('api/role')
@UseFilters(MongoExceptionFilter, HttpExceptionFilter)
@UseGuards(TokenGuard)
export class RoleController {
  constructor(
    private readonly roleService: RoleService,
  ) { }
  
  @Get()
  @TokenRequirements([TokenTypeEnum.system, TokenTypeEnum.client])
  public async getAll(@Req() request): Promise<PaginateResult<Role>> {    
    return this.roleService.findAll(request);
  }

  @Post()
  @TokenRequirements([TokenTypeEnum.system, TokenTypeEnum.client])
  public async create(@Body() CreateRoleDto: CreateRoleDto): Promise<Role> {
    return this.roleService.create(CreateRoleDto);
  }

  @Put(':roleid')
  @TokenRequirements([TokenTypeEnum.system, TokenTypeEnum.client])
  public async update(@Param('roleid') roleid: string, @Token() token: AccessToken, @Body() UpdateRoleDto: UpdateRoleDto): Promise<Role> {
    return this.roleService.update(roleid, UpdateRoleDto, token.uid);
  }

  @Delete(':roleid')
  @TokenRequirements([TokenTypeEnum.system, TokenTypeEnum.client])
  public async delete(@Param('roleid') roleid: string, @Token() token: AccessToken): Promise<Role> {
    return this.roleService.delete(roleid, token.uid);
  }
}
