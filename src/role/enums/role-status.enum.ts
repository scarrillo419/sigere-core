export enum RoleStatusEnum {
  active = 'active',
  inactive = 'inactive',
  remove = 'remove',
}

export const RoleStatusEnumAsArray = Object.keys(RoleStatusEnum);
