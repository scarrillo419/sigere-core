import { Document } from 'mongoose';
import { RoleStatusEnum } from '../enums/role-status.enum';
export interface Role extends Document {
  readonly _id: string;
  readonly name: string;
  readonly modules: string[];
  readonly status: RoleStatusEnum;    
  readonly created_at: Date;
}
