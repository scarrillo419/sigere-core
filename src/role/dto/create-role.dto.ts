import { IsString, IsNotEmpty, IsEnum, IsArray, ValidateNested, IsOptional } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';

import { RoleStatusEnum, RoleStatusEnumAsArray } from '../enums/role-status.enum';

export class RoleModule {
  @ApiProperty()
  @IsString()
  readonly key: string;

  @ApiProperty()
  @IsArray()
  readonly permissions: string[];
}

export class CreateRoleDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  readonly name: string;

  @ApiProperty()
  @ValidateNested({ each: true })
  @Type(() => RoleModule)
  @IsOptional()
  public modules: RoleModule[];

  @ApiProperty({ enum: RoleStatusEnumAsArray })
  @IsEnum(RoleStatusEnum, { each: true })
  @IsOptional()
  readonly status: RoleStatusEnum;
}
