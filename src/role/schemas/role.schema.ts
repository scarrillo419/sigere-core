import { Schema } from 'mongoose';
import { RoleStatusEnumAsArray, RoleStatusEnum } from '../enums/role-status.enum';

const ModuleSchema = new Schema({
  key: { type: String },
  permissions: [{ type: String }],
}, {
  _id: false,
});

export const RoleSchema = new Schema({
  name: { type: String },
  image: { type: String },
  modules: [{ type: ModuleSchema, default: [] }],
  status: { type: String, enum: RoleStatusEnumAsArray, default: RoleStatusEnum.active },
}, {
  timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' },
  // do not return certain fields when saving the document
  toObject: {
    transform: (doc, ret, options) => {
      // delete ret.password;
      return ret;
    },
  },
  toJSON: {
    transform: (doc, ret, options) => {
      delete ret.__v;
      return ret;
    },
  },
});