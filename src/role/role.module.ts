import { Module, forwardRef } from '@nestjs/common';
import { DatabaseModule } from '../common/database/database.module';
import { AuthModule } from '../auth/auth.module';
import { RoleController } from './role.controller';
import { roleProviders } from './role.providers';
import { RoleService } from './role.service';
import { UserModule } from '../user/user.module';

@Module({
  imports: [DatabaseModule, 
  forwardRef(() => AuthModule),
  forwardRef(() => UserModule)],
  controllers: [RoleController],
  providers: [RoleService, ...roleProviders],
  exports: [RoleService]
})
export class RoleModule { }
