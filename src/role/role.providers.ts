import { Connection } from 'mongoose';
import * as mongoosePaginate from 'mongoose-paginate';
import { RoleSchema } from './schemas/role.schema';

RoleSchema.plugin(mongoosePaginate);

export const roleProviders = [
  {
    provide: 'RoleModelToken',
    useFactory: (connection: Connection) => connection.model('Role', RoleSchema),
    inject: ['DbConnectionToken'],
  },
];
