import { BadRequestException, Injectable, Inject, NotFoundException, forwardRef } from '@nestjs/common';
import { Types as MongooseTypes, PaginateModel, PaginateOptions, PaginateResult, Model } from 'mongoose';
import { isEmpty } from 'lodash';

import { CreateRoleDto } from './dto/create-role.dto';
import { Role } from './interfaces/role.interface';
import { UpdateRoleDto } from './dto/update-role.dto';
import { RoleStatusEnum } from './enums/role-status.enum';
import { UserStatusEnum } from '../user/enums/user-status.enum';
import { UserService } from '../user/user.service';

@Injectable()
export class RoleService {
  constructor(
    @Inject('RoleModelToken') private readonly roleModel: PaginateModel<Role>,
    @Inject(forwardRef(() => UserService)) private readonly userService: UserService
  ) { }

  public async findAll(request: any): Promise<PaginateResult<Role>> {
    const { sortField, query, sortOrder } = request;

    const options: PaginateOptions = {
      limit: query.limit ? parseInt(query.limit) : 10,
      page: query.page ? parseInt(query.page) : 1,
      sort: sortField ? { [sortField]: sortOrder } : { createdAt: -1 },
    };

    const searchQuery: any = query.search ? JSON.parse(query.search) : {};

    if (searchQuery.email) searchQuery.email = { $regex: searchQuery.email, $options: 'i' };
    if (searchQuery.name) searchQuery.name = { $regex: searchQuery.name, $options: 'i' };
    if (searchQuery.childName) searchQuery.childName = { $regex: searchQuery.childName, $options: 'i' };
    if (searchQuery.createdAt) searchQuery.createdAt = { $gte: searchQuery.createdAt };
    if (searchQuery.dateLastLogin) searchQuery.dateLastLogin = { $gte: searchQuery.dateLastLogin };
    searchQuery.status = { $ne: RoleStatusEnum.remove };
    return this.roleModel.paginate({
      ...searchQuery,
    }, options);
  }

  public async findOne(criteria: object, projection?: string): Promise<Model<Role>> {
    if (!criteria) { throw new BadRequestException(); }

    const role = await this.roleModel.findOne(criteria, projection).exec();
    if (!role) { throw new NotFoundException('role.error.notFound'); }

    return role;
  }

  public async findOneById(roleId: MongooseTypes.ObjectId): Promise<Model<Role>> {
    return await this.findOne({ _id: MongooseTypes.ObjectId(roleId) });
  }

  public async create(CreateRoleDto: CreateRoleDto): Promise<Model<Role>> {
    const createdBase = new this.roleModel(CreateRoleDto);

    await createdBase.save();
    return createdBase;
  }

  public async update(roleId: MongooseTypes.ObjectId, updateRoleDto: UpdateRoleDto, userId: MongooseTypes.ObjectId): Promise<Model<Role>> {
    const dbRole = await this.findOneById(roleId) as PaginateModel<Role>;
    if (!dbRole) { throw new BadRequestException(); }

    if (!isEmpty(dbRole.modules) && updateRoleDto.modules === undefined) {
      dbRole.set({ updateRoleDto, modules: [] });
      // await this.userService.checkSelf(userId);
      return dbRole.save();
    }

    dbRole.set(updateRoleDto);
    // await this.userService.checkSelf(userId);

    return dbRole.save();
  }

  public async delete(roleId: MongooseTypes.ObjectId, userId: MongooseTypes.ObjectId): Promise<Role> {
    const dbRole = await this.findOneById(roleId);
    if (!dbRole) { throw new BadRequestException(); }

    dbRole.set({ status: RoleStatusEnum.remove });
    return dbRole.save();
  }

}
