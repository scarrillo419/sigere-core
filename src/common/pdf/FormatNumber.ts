import * as currencyFormatter from 'currency-formatter';
// import { CurrencyPrefixEnum } from '../../budget/enums/currency-prefix.enum';

const dacimals = {
    v1: 2,
    v2: 3,
    v3: 4,
    v4: 5,
    v5: 10,
};
const numericals = {
    v1: ',',
    v2: '.',
    v3: '',
    v4: '',
};

const decimals = {
    v1: '.',
    v2: ',',
    v3: '.',
    v4: ',',
};

// const getPrefix = (user) => {
//     return CurrencyPrefixEnum[user.preference.currency];
// };

const getNumberOfDecimal = (user) => {
    return dacimals[user.preference.decimals] || 2;
};

const getIntegerSeparator = (user) => {
    return numericals[user.preference.numericalFormat] || '.';
};

const getDecimalsSeparator = (user) => {
    return decimals[user.preference.numericalFormat] || ',';
};

const countDecimals = (value: any) =>  {
    if (Math.floor(value) === value) return 2;
    const numberOfZero = value.toString().split('.')[1].length;
    return numberOfZero <= 1 ? 2 : numberOfZero;
};


export const formattedNumber = (
        user,
        value,
    ) => {
    const prefix = '$';
    const suffix = '';
    const padRight = getNumberOfDecimal(user);
    const integerSeparator = getIntegerSeparator(user);
    const decimalsSeparator = getDecimalsSeparator(user);

    const numberFormateD = parseFloat(value);
    const countDecimal = countDecimals(numberFormateD);
    const formattedNumber = currencyFormatter.format(Number(value), {
        symbol: prefix || suffix,
        decimal: decimalsSeparator,
        thousand: integerSeparator,
        precision: countDecimal > padRight ? padRight : countDecimal,
        format: {
            pos: `${prefix && '%s'}%v${suffix && '%s'}`,
            neg: `(${prefix && '%s'}%v${suffix && '%s'})`,
            zero: `${prefix && '%s'}%v${suffix && '%s'}`,
          },
    });

    return formattedNumber;
};

export const removeRightZeros = (settings: any, value: any) => {
    const _value = parseFloat(value);
    const padRight = getNumberOfDecimal(settings);
    const countDecimal = countDecimals(_value);
    const precision = countDecimal > padRight ? padRight : countDecimal;
    const svalue = _value.toFixed(precision);
    return +svalue;
};