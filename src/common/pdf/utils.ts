const moment = require('moment');

export const financial = (x) => {
  return Number.parseFloat(x);
};

export const clearAddress = (val: string) => val.replace(/(\r\n|\n|\r)/gm, '');

export const dateFormat = (val: string | Date | undefined, format?: string, formatOrigin?: string) =>
  moment(val, formatOrigin).format((format || 'YYYY-MM-DD'));
