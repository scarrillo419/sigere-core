// import { Types as MongooseTypes } from 'mongoose';
const mongoose = require('mongoose');

export const defaultStages = (lang: any, i18n: any) => {
  return [
    {
      _id: mongoose.Types.ObjectId(),
      name: i18n.translate(lang, 'funnel.stage1'),
      description: '',
      leads: [],
    },
    {
      _id: mongoose.Types.ObjectId(),
      name: i18n.translate(lang, 'funnel.stage2'),
      description: '',
      leads: [],
    },
    {
      _id: mongoose.Types.ObjectId(),
      name: i18n.translate(lang, 'funnel.stage3'),
      description: '',
      leads: [],
    },
    {
      _id: mongoose.Types.ObjectId(),
      name: i18n.translate(lang, 'funnel.stage4'),
      description: '',
      leads: [],
    },
    {
      _id: mongoose.Types.ObjectId(),
      name: i18n.translate(lang, 'funnel.stage5'),
      description: '',
      leads: [],
    },
  ];
};
