export const AuthConstants = {
  cert: {
    privateKey: 'ZhsgKYjI5WE4fbuOOyiCSkwqObGpnmaJ',
    publicKey: 'ZhsgKYjI5WE4fbuOOyiCSkwqObGpnmaJ',
  },
  access_token: {
    options: {
      algorithm: 'RS256',
      expiresIn: '15min',
      issuer: 'nestjs-auth-example',
    },
  },
  refresh_token: {
    length: 64,
    expiresIn: '30 days',
  },
};
