import { ConfigService } from '../config/config.service';

const configService = new ConfigService('.env');

export const DatabaseConstants = {
  uri: configService.get('MONGO_URI'),
  encryptor: {
    key: 'ZhsgKYjI5WE4fbuOOyiCSkwqObGpnmaJ',
  },
};
