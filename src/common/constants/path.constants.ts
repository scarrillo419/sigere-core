import { join } from 'path';
export const publicPath = {
  users: join(__dirname, '../../../public/users'),
  employees: join(__dirname, '../../../public/employees'),
  products: join(__dirname, '../../../public/product'),
  digitalSignature: join(__dirname, '../../../public/digital-signature'),
  templates: join(__dirname, '../../../public/templates'),
  lead: join(__dirname, '../../../public/lead'),
  deliveryNote: join(__dirname, '../../../public/delivery-note'),
  proforma: join(__dirname, '../../../public/proforma'),
};

export const fonts = {
  Roboto: {
    normal: join(__dirname, '..', './resources/fonts/Roboto-Regular.ttf'),
    bold: join(__dirname, '..', './resources/fonts/Roboto-Medium.ttf'),
    italics: join(__dirname, '..', './resources/fonts/Roboto-Italic.ttf'),
    bolditalics: join(__dirname, '..', './resources/fonts/Roboto-MediumItalic.ttf'),
    templates: join(__dirname, '../../../public/templates'),
  }
};
