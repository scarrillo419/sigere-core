import * as mongoose from 'mongoose';

const { Schema } = mongoose;

export enum HistoryType {
    create = 'create',
    update = 'update',
    remove = 'remove',
}

const historyTypeAsArray = Object.keys(HistoryType);

const UserSchema = new Schema({
    name: { type: String },
    lastName: { type: String },  
    email: { type: String, required: true },
});

export const HistorySchema = new Schema({
    collectionName: { type: String, required: true },
    documentId: { type: Schema.Types.ObjectId, required: true },
    diff: {},
    type: { type: String, enum: historyTypeAsArray, required: true },
    version: { type: Number, min: 0 }
  }, {
    timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' },
    // do not return certain fields when saving the document
    toObject: {
      transform: (doc, ret, options) => {
        return ret;
      },
    },
    toJSON: {
      transform: (doc, ret, options) => {
        return ret;
      },
    },
  });

  export const HistorySchemaModel = mongoose.model('History', HistorySchema);