import * as mongoose from 'mongoose';
import * as omit from 'omit-deep';
import { pick } from 'lodash';
import { assign } from 'power-assign';
import * as empty from 'deep-empty-object';
import { HistoryType, HistorySchemaModel } from './history.schema';

// try to find an id property, otherwise just use the index in the array
const objectHash = (obj, idx) => obj._id || obj.id || `$$index: ${idx}`;
const diffPatcher = require('jsondiffpatch').create({ objectHash });

export function historyPlugin(schema, opts: any = {}) {

    if (opts.omit && !Array.isArray(opts.omit)) {
        if (typeof opts.omit === 'string') {
            opts.omit = [opts.omit];
        } else {
            const errMsg = `opts.omit expects string or array, instead got '${typeof opts.omit}'`;
            throw new TypeError(errMsg);
        }
    }

    schema.pre('save', function (next) {
        const type = this.isNew ? HistoryType.create : HistoryType.update;
        this.constructor.findOne({ _id: this._id })
            .then((original) => {
                return saveDiffObject(this, original, this.toObject({ depopulate: true }), opts, type, null);
            })
            .then(() => next())
            .catch(next);
    });

    schema.pre('findOneAndUpdate', function (next) {
        saveDiffs(this, opts, HistoryType.update)
            .then(() => next())
            .catch(next);
    });

    schema.pre('update', function (next) {
        saveDiffs(this, opts, HistoryType.update)
            .then(() => next())
            .catch(next);
    });

    schema.pre('updateOne', function (next) {
        saveDiffs(this, opts, HistoryType.update)
            .then(() => next())
            .catch(next);
    });

    schema.pre('remove', function (next) {
        saveDiffObject(this, this, {}, opts, HistoryType.remove)
            .then(() => next())
            .catch(next);
    });
}


function saveDiffObject(currentObject, original, updated, opts, type: HistoryType, queryObject?) {

    let diff = diffPatcher.diff(
        JSON.parse(JSON.stringify(original)),
        JSON.parse(JSON.stringify(updated))
    );

    omit(diff, ['updatedAt', 'createdAt'], { cleanEmpty: true });

    if (opts.omit) {
        omit(diff, opts.omit, { cleanEmpty: true });
    }

    if (opts.pick) {
        diff = pick(diff, opts.pick);
    }

    if (!diff || !Object.keys(diff).length || empty.all(diff)) {
        return;
    }

    const documentId = currentObject._id;
    const collectionName = currentObject.constructor.modelName || queryObject.model.modelName;

    return HistorySchemaModel.findOne({ documentId, collectionName })
        .sort('-version')
        .then(lastHistory => {
            const history = new HistorySchemaModel({
                documentId,
                collectionName,
                diff,
                type,
                version: lastHistory ? lastHistory.version + 1 : 0,
            });
            return history.save();
        });
}

const saveDiffs = (queryObject, opts, type: HistoryType) =>
    queryObject
        .find(queryObject._conditions)
        .lean(false)
        .cursor()
        .eachAsync(result => saveDiffHistory(queryObject, result, opts, type));

const saveDiffHistory = (queryObject: any, currentObject: any, opts: any, type: HistoryType) => {
    const update: any = JSON.parse(JSON.stringify(queryObject._update));
    const updateKeys = Object.keys(update).map((key: any) => {
        if (typeof update[key] === 'object') {
            return update[key];
        }
        return update;
    });
    const updateParams: any = Object.assign(updateKeys);
    delete queryObject._update['$setOnInsert'];
    const dbObject = pick(currentObject, Object.keys(updateParams));
    return saveDiffObject(
        currentObject,
        dbObject,
        assign(dbObject, queryObject._update),
        opts,
        type,
        queryObject
    );
};