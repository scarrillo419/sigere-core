import { Connection } from 'mongoose';
import * as mongoosePaginate from 'mongoose-paginate';
import { HistorySchema } from './history.schema';

HistorySchema.plugin(mongoosePaginate);
export const historyProviders = [
  {
    provide: 'HistoryModelToken',
    useFactory: (connection: Connection) => connection.model('History', HistorySchema),
    inject: ['DbConnectionToken'],
  },
];
