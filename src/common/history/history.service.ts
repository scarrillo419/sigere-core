import { Injectable, Inject } from '@nestjs/common';
import { PaginateModel, PaginateOptions, PaginateResult } from 'mongoose';
import { History } from './history.interface';

@Injectable()
export class HistoryService {
  constructor(
    @Inject('HistoryModelToken') public readonly historyModel: PaginateModel<History>
  ) { }
  public async findAll(options: PaginateOptions, searchQuery): Promise<PaginateResult<History>> {

    return this.historyModel.paginate({
      ...searchQuery
    }, options);
  }
}
