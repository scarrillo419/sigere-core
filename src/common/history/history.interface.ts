import { Document } from 'mongoose';

export interface History extends Document {
  readonly _id: string;
}
