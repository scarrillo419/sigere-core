import { Module } from '@nestjs/common';
import { historyProviders } from './history.providers';
import { DatabaseModule } from '../../common/database/database.module';
import { HistoryService } from './history.service';


@Module({
  imports: [
    DatabaseModule,
  ],
  providers: [HistoryService, ...historyProviders],
  exports: [HistoryService],
})
export class HistoryModule { }
