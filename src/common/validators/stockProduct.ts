import { ValidatorConstraint, ValidatorConstraintInterface, ValidationArguments } from 'class-validator';
import { isEmpty } from 'lodash';
import * as mongoose from 'mongoose';

@ValidatorConstraint({ async: true })
export class StockProduct implements ValidatorConstraintInterface {

  async validate(model: string, args: ValidationArguments): Promise<any> {
    const mongoModel = mongoose.model('Product');
    const criteria = {};
    criteria[args.property] = args.object['items'];

    if (args.object['items'].length > 0) {
      const dbProps = await mongoModel.find({ _id: { $in: args.object['items'].map(p => p._id) }, status: { $ne: 'removed' } }).exec();

      // validate stock additionals
      const validateR = [];
      const loop = (data) => {
        const validate = data.map(async (item) => {
          if (item.additionals) {
            const dbAdditionals = await mongoModel.find({
              _id: { $in: item.additionals.map(p => p._id) },
              status: { $ne: 'removed' }
            }).exec();
            // validate stock
            for (const prop of item.additionals) {
              const validate = dbAdditionals.filter((doc) => {
                if (!doc.withoutStock && (doc._id.toString() === prop._id.toString())) {
                  return prop.quantity > doc.stock;
                }
              });
              validateR.push(isEmpty(validate));
              return await Promise.all(loop(dbAdditionals));
            }
          }
          return validateR;
        });
        return validate;
      };

      // const validate = loop(dbProps);
      await Promise.all(loop(dbProps));
      for (const [i, prop] of validateR.entries()) {
        if (!prop) return prop;
      }

      const holder = {};
      args.object['items'].forEach((d) => {
        if (holder.hasOwnProperty(d._id)) {
          holder[d._id] = holder[d._id] + d.quantity;
        } else {
          holder[d._id] = d.quantity;
        }
      });
      const obj2 = [];

      for (const prop in holder) {
        obj2.push({ _id: prop, quantity: holder[prop] });
      }
      for (const prop of obj2) {
        const validate = dbProps.filter((doc) => {
          if (!doc.withoutStock && (doc._id.toString() === prop._id.toString())) {
            return prop.quantity > doc.stock;
          }
        });
        return isEmpty(validate);
      }
    } else {
      return true;
    }
  }
}