import { SetMetadata } from '@nestjs/common';
import { uniq } from 'lodash';
import { TokenTypeEnum } from '../../token/enums/token-type.enum';
import { ModulesKeyEnum } from '../../user/enums/modules-key.enum';
import { RoleOptionsEnum } from '../../user/enums/role-options.enum';

export const TokenRequirements =
 (requiredTokenType: TokenTypeEnum[], requiredUserRoles?: ModulesKeyEnum[], requiredUserpermissions?: RoleOptionsEnum[]) =>
 SetMetadata('tokenrequirements', new TokenRequirementsHelper(requiredTokenType, requiredUserRoles, requiredUserpermissions));

export class TokenRequirementsHelper {

  private requiredTokenType: TokenTypeEnum[];
  public requiredUserRoles: ModulesKeyEnum[];
  public requiredUserpermissions: RoleOptionsEnum[];

  constructor(requiredTokenType: TokenTypeEnum[], requiredUserRoles: ModulesKeyEnum[], requiredUserpermissions: RoleOptionsEnum[]) {
    this.requiredTokenType = requiredTokenType;
    this.requiredUserRoles = requiredUserRoles;
    this.requiredUserpermissions = requiredUserpermissions;
  }

  public tokenIsOfType(tokenType: TokenTypeEnum): Boolean {
    return this.requiredTokenType.some(requiredType => tokenType === requiredType);
  }

  public tokenHasAllUserRoles(userRoles: any[]): Boolean {
    if (this.requiredUserRoles) {
      const roleModules = userRoles.map(item => item.key);
      return this.requiredUserRoles.some(requiredRole => roleModules.indexOf(requiredRole) >= 0);
    }
    return true;
  }

  public tokenHasAllUserPermissions(userPermissions: any[]): Boolean {
    if (this.requiredUserpermissions) {
      const permissions: any[] = [];
      userPermissions.filter(item => this.requiredUserRoles.includes(item.key)).forEach(item => {
        permissions.push(...item.permissions);
      });
      return this.requiredUserpermissions.some(requiredPermission => uniq(permissions).indexOf(requiredPermission) >= 0);
    }
    return true;
  }

}
