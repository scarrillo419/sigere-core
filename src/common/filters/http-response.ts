import { ArgumentsHost, HttpException } from '@nestjs/common';

export const httpResponse = (exception: HttpException, host: ArgumentsHost): void => {
  const ctx = host.switchToHttp();
  const res = ctx.getResponse();
  const status = exception.getStatus();
  let message = exception.getResponse() as any;
  
  if (message.message === undefined) { message = message.error; }
  else message = message.message;

  res.status(status).json({
    error: {
      message,
      code: status,
    },
  });
};
