import * as mongoose from 'mongoose';
// import { MongoMemoryServer } from 'mongodb-memory-server';
import { DatabaseConstants } from '../constants/database.constants';

export const databaseProviders = [
  {
    provide: 'DbConnectionToken',
    useFactory: async () => {
      (mongoose as any).Promise = global.Promise;
      // if (process.env.NODE_ENV === 'test') {
      //   const mockgoose = new MongoMemoryServer(mongoose);
      //   const mongoUri = await mockgoose.getConnectionString();
      //   await mongoose.connect(mongoUri, { useNewUrlParser: true });
      // } else {
      await mongoose.connect(DatabaseConstants.uri, { useNewUrlParser: true });
      // }
      return mongoose;
    },
  },
];
