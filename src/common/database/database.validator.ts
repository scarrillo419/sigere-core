import { ValidatorConstraint, ValidatorConstraintInterface, ValidationArguments } from 'class-validator';
import { isEmpty } from 'lodash';

import * as mongoose from 'mongoose';

@ValidatorConstraint({ async: true })
export class Exist implements ValidatorConstraintInterface {

    async validate(value: string, args: ValidationArguments): Promise<any> {
        const mongoModel = mongoose.model(args.constraints[0]);
        const criteria = { };
        criteria[args.property] = args.object[args.property];
        const result = await mongoModel.findOne(criteria).exec();

        return isEmpty(result);
    }

    defaultMessage(args: ValidationArguments) {
        return 'field ($value) is no exist!';
    }

}

@ValidatorConstraint({ async: true })
export class NoExist implements ValidatorConstraintInterface {

    async validate(model: string, args: ValidationArguments): Promise<any> {
        const mongoModel = mongoose.model(args.constraints[0]);
        const criteria = { };

        if (args.constraints.length > 1) {
            criteria[`${args.constraints[1]}.${args.property}`] = args.object[args.property];
        }
        else criteria[args.property] = args.object[args.property];
        const result = await mongoModel.findOne(criteria).exec();
        return isEmpty(result);
    }

    defaultMessage(args: ValidationArguments) {
        return '$property ($value) exist!';
    }

}

@ValidatorConstraint({ async: true })
export class NoExistAnother implements ValidatorConstraintInterface {

    async validate(value: string, args: ValidationArguments): Promise<any> {
        const mongoModel = mongoose.model('User');
        const criteria = { };
        criteria[args.property] = args.object[args.property];
        const result = await mongoModel.findOne(criteria).exec();
        return isEmpty(result);
    }

    defaultMessage(args: ValidationArguments) {
        return '$property ($value) exist!';
    }

}
