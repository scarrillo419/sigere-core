import { ConfigService } from '../config/config.service';

const configService = new ConfigService('.env');

export const Mailjet = require('node-mailjet').connect(
  configService.get('MJ_APIKEY_PUBLIC'),
  configService.get('MJ_APIKEY_PRIVATE'),
);

export const MailjetSender = configService.get('MJ_SENDER');

export const WebUrl = configService.get('WEB_URL');