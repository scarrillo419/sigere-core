import { Mailjet, MailjetSender, WebUrl } from '.';
import { User } from '../../user/interfaces/user.interface';

export const newUserMail = (user: User, password: string, token: string) => {
  const options = {
    'Messages': [
      {
        'From': {
          'Email': MailjetSender,
          'Name': ''
        },
        'To': [
          {
            'Email': user.email,
            'Name': user.name
          }
        ],
        'Subject': 'Bienvenido',
        'TemplateID': 1232746,
        'TemplateLanguage': true,
        'Variables': {
          'email': user.email,
          'password': password,
          'token': `${WebUrl}/authentication/verify-user/${token}`,
          // 'token': randString ,
        },
      }
    ]
  };

  Mailjet.post('send', { 'version': 'v3.1' }).request(options);
};

export const newTokenMail = (user: User, token: string) => {
  const options = {
    'Messages': [
      {
        'From': {
          'Email': MailjetSender,
          'Name': ''
        },
        'To': [
          {
            'Email': user.email,
            'Name': user.name
          }
        ],
        'Subject': 'Bienvenido',
        'TemplateID': 1261451,
        'TemplateLanguage': true,
        'Variables': {
          'email': user.email,
          'token': `${WebUrl}/authentication/verify-user/${token}`,
          // 'token': randString ,
        },
      }
    ]
  };

  Mailjet.post('send', { 'version': 'v3.1' }).request(options);
};
