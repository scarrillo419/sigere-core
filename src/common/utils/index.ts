import { isEmpty } from 'lodash';

export async function generateDocumentNumber(codeModel: any, documentNumber: string, prefix: string, edit: boolean, notSave: boolean = false) {
  if (!isEmpty(documentNumber) && !notSave) {
    const code = await codeModel.findOne({documentNumber});
    if (!code) return new codeModel({documentNumber}).save();
    if (edit) return code;
  }

  const number = (await codeModel.find().count() + 1).toString();
  let newCode = '';

  if (number.length >= 4) newCode = `${prefix}${number}`;
  if (number.length === 3) newCode = `${prefix}0${number}`;
  if (number.length === 2) newCode = `${prefix}00${number}`;
  if (number.length === 1) newCode = `${prefix}000${number}`;

  if (notSave) return { documentNumber: newCode }; // Cuando se entra al formulario, enviar el proximo numero de documento

  return new codeModel({documentNumber: newCode}).save();
}

export async function generateProductCode(model: any, name: string, props: any) {
  let code = '';
  let codeCounter = 1;
  let products = [];
  let objSearch = {};

  const arrName = name.split(' ');
  for (const n of arrName) {
    code += n.charAt(0).toUpperCase();
  }

  if (!isEmpty(props)) {
    props.forEach((prop, index) => {
      code += prop.value.toString();
      objSearch = { ...objSearch, [`props.${index}._id`]: prop._id, [`props.${index}.value`]: Number(prop.value) };
    });

    products = await model.find({
      $and: [objSearch, {props: { $size: props.length }}, {codeString: code}],
    });
  }
  const codeString = code;

  if (isEmpty(products)) code += `00${codeCounter}`;
  else {
    codeCounter = products.length + 1;
    const number = codeCounter.toString();
    if (number.length >= 3) code += codeCounter;
    if (number.length === 2) code += `0${codeCounter}`;
    if (number.length === 1) code += `00${codeCounter}`;
  }

  return { code, codeCounter, codeString };
}

export async function getHistoryStage(dbItem: any, stageId: string) {
  const len = dbItem.funnel.stages ? dbItem.funnel.stages.length : -1;
  let history = dbItem.historyStage || {};
  let found = false;
  let i = 0;

  if (len === -1) {
    history = { ...history, [stageId]: new Date() };
  } else {
    for (const stage of dbItem.funnel.stages) {
      if (stage._id.toString() !== stageId) {
        if (!found) {
          if (!dbItem.historyStage[stage._id]) {
            history = { ...history, [stage._id]: new Date() };
          }
        } else {
          delete history[stage._id];
        }
      } else {
        if (i < (len - 1)) found = true;
        history = { ...history, [stage._id]: new Date() };
      }
      i += 1;
    }
  }

  return history;
}
