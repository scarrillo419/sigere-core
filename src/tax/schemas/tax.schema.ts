import { Schema } from 'mongoose';
import { TaxStatusEnumAsArray } from '../enums/tax-status.enum';
import { TaxTypeEnumAsArray } from '../enums/tax-type-enum';
import { TaxAmbitEnumAsArray } from '../enums/tax-ambit.enum';
import { GroupTaxEnumAsArray } from '../enums/tax-group.enum';

export const TaxSchema = new Schema({
  name: { type: String, required: true },
  value: { type: Number },
  ambit: { type: String, required: true, enum: TaxAmbitEnumAsArray },
  group: { type: String , enum: GroupTaxEnumAsArray},
  status: { type: String, required: true, enum: TaxStatusEnumAsArray },
  type: { type: String, required: true, enum: TaxTypeEnumAsArray },
}, {
  timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' },
  // do not return certain fields when saving the document
  toObject: {
    transform: (doc, ret, options) => {      
      return ret;
    },
  },
  toJSON: {
    transform: (doc, ret, options) => {
      delete ret.__v;
      return ret;
    },
  },
});

export const TaxBasicSchema = new Schema({
  name: { type: String },
  value: { type: Number },
});

