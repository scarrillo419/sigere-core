import { BadRequestException, Injectable, Inject, NotFoundException, forwardRef } from '@nestjs/common';
import { Types as MongooseTypes, PaginateModel, PaginateOptions, PaginateResult, Model } from 'mongoose';
import { CreateTaxDto } from './dto/create-tax.dto';
import { Tax } from './interfaces/tax.interface';
import { UpdateTaxDto } from './dto/update-tax.dto';
import { UserService } from '../user/user.service';
import { TaxStatusEnum } from './enums/tax-status.enum';

@Injectable()
export class TaxService {

  constructor(
    @Inject(forwardRef(() => UserService)) private readonly userService: UserService,
    @Inject('TaxModelToken') public readonly taxModel: PaginateModel<Tax>
  ) { }

  /**
   * Get all taxes
   * @return {Tax[]} A list containing all taxes
   */
  public async findAll(userid: MongooseTypes.ObjectId, request: any): Promise<PaginateResult<Tax>> {
    const { sortField, query, sortOrder } = request;

    const options: PaginateOptions = {
      limit: query.limit ? parseInt(query.limit) : 10,
      page: query.page ? parseInt(query.page) : 1,
      sort: sortField ? { [sortField]: sortOrder } : { createdAt: -1 },
    };

    const searchQuery: any = query.search ? JSON.parse(query.search) : {};

    if (searchQuery.name) searchQuery.name = { $regex: searchQuery.name, $options: 'i' };
    if (searchQuery.value) searchQuery.value =  searchQuery.value;
    if (searchQuery.key) searchQuery.key = { $regex: searchQuery.key, $options: 'i' };
    
    if (searchQuery.ambit) searchQuery.ambit = searchQuery.ambit;
    if (searchQuery.group) searchQuery.group =  searchQuery.group;
    searchQuery.status = { $ne : TaxStatusEnum.remove };
    if (searchQuery.status) searchQuery.status =  searchQuery.status;

    return this.taxModel.paginate({
      ...searchQuery,
    }, options);
  }

  /**
   * Get a single tax through defined criterias
   * @param {object} criteria - The defined criterias
   * @param {string} projection - Optional projection field (i.e. which fields to return)
   * @return {Tax} A single tax
   */
  public async findOne(criteria: object, projection?: string): Promise<Model<Tax>> {
    if (!criteria) { throw new BadRequestException(); }
    const tax = await this.taxModel.findOne(criteria, projection).exec();
    if (!tax) { throw new NotFoundException(); }
    return tax;
  }

  /**
   * Get a single tax through its identifier
   * @param {ObjectId} taxid - The mongodb object id
   * @return {Tax} A single tax
   */
  public async findOneById(taxid: MongooseTypes.ObjectId): Promise<Model<Tax>> {
    return await this.findOne({ _id: taxid });
  }
  /**
    * Create a new tax
    * @param {CreateTaxDto} createTaxDto - A data-transfer-object describing the new tax
    * @return {Tax} The newly created tax
    */
  public async create(createTaxDto: CreateTaxDto, userid: MongooseTypes.ObjectId): Promise<Model<Tax>> {

    const createdTax = await new this.taxModel(createTaxDto);

    await createdTax.save();
    return createdTax;
  }
  /**
 * Update an existing tax
 * @param {ObjectId} taxid - The mongodb object id
 * @param {UpdateTaxDto} updateTaxDto - A data-transfer-object describing the tax to be updated
 * @return {Tax} The updated tax
 */
  public async update(taxid: MongooseTypes.ObjectId, updateTaxDto: UpdateTaxDto): Promise<Model<Tax>> {
    const dbTax = await this.findOneById(taxid) as PaginateModel<Tax>;
    if (!dbTax) { throw new BadRequestException(); }

    dbTax.set(updateTaxDto);
    await dbTax.save();

    return dbTax;
  }
  /**
   * Delete an existing tax
   * @param {ObjectId} taxid - The mongodb object id
   * @return {Tax} The deleted tax
   */
  public async delete(taxid: MongooseTypes.ObjectId): Promise<Tax> {
    const dbTax = await this.findOneById(taxid);
    if (!dbTax) { throw new BadRequestException(); }

    dbTax.status = TaxStatusEnum.remove;
    return dbTax.save();
  }

}
