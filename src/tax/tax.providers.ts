import { Connection } from 'mongoose';
import * as mongoosePaginate from 'mongoose-paginate';
import { TaxSchema } from './schemas/tax.schema';

TaxSchema.plugin(mongoosePaginate);

export const taxProviders = [
  {
    provide: 'TaxModelToken',
    useFactory: (connection: Connection) => connection.model('Tax', TaxSchema),
    inject: ['DbConnectionToken'],
  },
];
