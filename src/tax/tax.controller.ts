import { Body, Controller, Delete, Get, Param, Post, Put,
  UseFilters, UseGuards, Req  } from '@nestjs/common';
import { PaginateResult } from 'mongoose';
import { ApiTags, ApiBearerAuth, } from '@nestjs/swagger';
import { HttpExceptionFilter } from '../common/filters/http-exception.filter';
import { MongoExceptionFilter } from '../common/filters/mongo-exception.filter';
import { TaxService } from './tax.service';
import { CreateTaxDto } from './dto/create-tax.dto';
import { Tax } from './interfaces/tax.interface';
import { TokenGuard } from '../common/guards/token.guard';
import { UpdateTaxDto } from './dto/update-tax.dto';
import { TokenTypeEnum } from '../token/enums/token-type.enum';
import { AccessToken } from '../token/interfaces/access-token.interface';
import { TokenRequirements } from '../common/decorators/token-requirements.decorator';
import { Token } from '../common/decorators/token.decorator';
import { ModulesKeyEnum } from '../user/enums/modules-key.enum';
import { RoleOptionsEnum } from '../user/enums/role-options.enum';

@ApiTags('Tax')
@ApiBearerAuth()
@Controller('api/tax')
@UseFilters(MongoExceptionFilter, HttpExceptionFilter)
@UseGuards(TokenGuard)
export class TaxController {
  constructor(
    private readonly taxService: TaxService,
    ) { }
  /**
   * Get all taxes
   * @return {Tax[]} A list containing all taxes
   */
  @Get()
  @TokenRequirements([TokenTypeEnum.system, TokenTypeEnum.client],
    [ModulesKeyEnum.taxes, ModulesKeyEnum.budget, ModulesKeyEnum.salesInvoice, ModulesKeyEnum.delivery],
    [RoleOptionsEnum.view])
  public async getAll(@Token() token: AccessToken, @Req() request): Promise<PaginateResult<Tax>> {    
    return this.taxService.findAll(token.uid , request);
  }

  /**
   * Create a new tax
   * @param {CreateTaxDto} createTaxDto - A data-transfer-object describing the new tax
   * @return {Tax} The newly created tax
   */
  @Post()
  @TokenRequirements([TokenTypeEnum.system, TokenTypeEnum.client],
    [ModulesKeyEnum.taxes],
    [RoleOptionsEnum.create])
  public async create(@Token() token: AccessToken, @Body() createTaxDto: CreateTaxDto): Promise<Tax> {
    return this.taxService.create(createTaxDto, token.uid);
  }

  /**
   * Update an existing tax
   * @param {ObjectId} taxid - The tax id
   * @param {UpdateTaxDto} updateTaxDto - A data-transfer-object describing the tax to be updated
   * @return {Tax} The updated tax
   */
  @Put(':taxid')
  @TokenRequirements([TokenTypeEnum.system, TokenTypeEnum.client],
    [ModulesKeyEnum.taxes],
    [RoleOptionsEnum.edit])  
  public async update(@Param('taxid') taxid: string,
                      @Body() updateTaxDto: UpdateTaxDto ): Promise<Tax> {
    return this.taxService.update(taxid, updateTaxDto, );
  }

  /**
   * Delete an existing tax 
   * @param {ObjectId} taxid - The tax id
   * @return {Tax} The deleted tax
   */
  @Delete(':taxid')
  @TokenRequirements([TokenTypeEnum.system, TokenTypeEnum.client],
    [ModulesKeyEnum.taxes],
    [RoleOptionsEnum.delete]) 
  public async delete(@Param('taxid') taxid: string): Promise<Tax> {
    return this.taxService.delete(taxid);
  }

}
