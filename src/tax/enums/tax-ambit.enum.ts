export enum TaxAmbitEnum {
    shopping = 'shopping',
    sales = 'sales',
    none = 'none'
  }
  
  export const TaxAmbitEnumAsArray = Object.keys(TaxAmbitEnum);
  