export enum TaxStatusEnum {
  active = 'active',
  inactive = 'inactive',
  remove = 'remove',
}

export const TaxStatusEnumAsArray = Object.keys(TaxStatusEnum);
