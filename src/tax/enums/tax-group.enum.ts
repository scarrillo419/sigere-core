export enum GroupTaxEnum {
    none = 'none',
    vat = 'vat',
    incomeTax = 'incomeTax',
    others = 'others',
}

export const GroupTaxEnumAsArray = Object.keys(GroupTaxEnum);
