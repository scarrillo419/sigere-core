import { Document } from 'mongoose';
import { TaxStatusEnum } from '../enums/tax-status.enum';
import { TaxTypeEnum } from '../enums/tax-type-enum';
import { TaxAmbitEnum } from '../enums/tax-ambit.enum';

export interface Tax extends Document {
  readonly _id: string;
  readonly name: string;
  readonly value: number;
  readonly ambit: TaxAmbitEnum;
  readonly key: string;
  readonly group: string;
  readonly type: TaxTypeEnum ;
  readonly status: TaxStatusEnum;    
  readonly created_at: Date;
 
}
