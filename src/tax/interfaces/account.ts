export interface Account extends Document {
    readonly id: string;
    readonly name: string;
    readonly account: number;
    readonly type: string;
    readonly color: any;
}