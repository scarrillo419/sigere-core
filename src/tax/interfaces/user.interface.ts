
export interface User extends Document {
    readonly id: string;
    readonly name: string;
    readonly lastName: string;
    readonly email: string;
}