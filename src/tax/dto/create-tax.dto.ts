import { IsString, IsEnum, IsNumber, IsOptional } from 'class-validator';
import { Type } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';
import { TaxStatusEnum, TaxStatusEnumAsArray } from '../enums/tax-status.enum';
import { TaxTypeEnum, TaxTypeEnumAsArray } from '../enums/tax-type-enum';
import { TaxAmbitEnum, TaxAmbitEnumAsArray } from '../enums/tax-ambit.enum';
import { GroupTaxEnumAsArray, GroupTaxEnum } from '../enums/tax-group.enum';
import { User } from './user-tax.dto';

export class CreateTaxDto {
  @ApiProperty()
  @IsString()
  readonly name : string;

  @ApiProperty()
  @IsNumber()
  readonly value : number;

  @ApiProperty({ enum: GroupTaxEnumAsArray })
  @IsEnum(GroupTaxEnum, { each: true })  
  readonly group: GroupTaxEnum;

  @ApiProperty({ enum: TaxAmbitEnumAsArray })
  @IsEnum(TaxAmbitEnum, { each: true })  
  readonly ambit : TaxAmbitEnum; 

  @ApiProperty({ enum: TaxTypeEnumAsArray })
  @IsEnum(TaxTypeEnum, { each: true })  
  readonly type: TaxTypeEnum;

  @ApiProperty({ enum: TaxStatusEnumAsArray })
  @IsEnum(TaxStatusEnum, { each: true })  
  readonly status: TaxStatusEnum;

}
