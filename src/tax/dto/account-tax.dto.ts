import { IsString, IsInt } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class Account {

    @ApiProperty()
    @IsString()
    readonly _id: string;
  
    @ApiProperty()
    @IsString()
    readonly name: string;
  
    @ApiProperty()
    @IsString()
    readonly account: string;

    @ApiProperty()
    @IsString()
    readonly accountRectificative: string;
  
    @ApiProperty()
    @IsString()
    readonly type: string;

    @ApiProperty()
    @IsString()
    readonly color: string;

}