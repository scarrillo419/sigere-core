import { Module, forwardRef } from '@nestjs/common';
import { DatabaseModule } from '../common/database/database.module';
import { AuthModule } from '../auth/auth.module';
import { TaxController } from './tax.controller';
import { taxProviders } from './tax.providers';
import { TaxService } from './tax.service';
import { UserModule } from '../user/user.module';

@Module({
  imports: [DatabaseModule,
    forwardRef(() => AuthModule),
    forwardRef(() => UserModule),
  ],
  controllers: [TaxController],
  providers: [TaxService, ...taxProviders],
  exports: [TaxService]
})
export class TaxModule { }
