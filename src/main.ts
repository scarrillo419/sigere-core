import { NestFactory } from '@nestjs/core';
import { NestExpressApplication } from '@nestjs/platform-express';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { join } from 'path';
import { AppModule } from './app.module';
import { NetworkConstants } from './common/constants/network.constants';

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule, { cors: true });

  const options = new DocumentBuilder()
    .setTitle('Ascario Solutions')
    .setDescription('The Core API description')
    .setVersion('1.0')
    .addTag('Core')
    .addBearerAuth()
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('/api/docs', app, document);

  app
  // .enableCors()
  .useStaticAssets(join(__dirname, '../public'), {
    prefix: '/api/public/',
    index: false,
    redirect: false,
  });

  await app.listen(NetworkConstants.port);
}
bootstrap();
