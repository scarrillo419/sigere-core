import { IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CrooperDto {

  @ApiProperty()
  @IsString()
  readonly x: number;

  @ApiProperty()
  @IsString()
  readonly y: number;

  @ApiProperty()
  @IsString()
  readonly width: number;

  @ApiProperty()
  @IsString()
  readonly height: number;
}