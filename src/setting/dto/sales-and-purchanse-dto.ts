import { IsOptional, IsEnum } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { ExpirationSettingsEnum, ExpirationSettingsEnumAsArray } from '../../user/enums/user-expiration.enum';

export class SalesAndPurchanse {

  @ApiProperty({ enum: ExpirationSettingsEnumAsArray })
  @IsOptional()
  readonly expiration: ExpirationSettingsEnum;

  @ApiProperty()
  @IsOptional()
  public saleTax: string;

  @ApiProperty()
  @IsOptional()
  public purchaseTax: string;

  @ApiProperty()
  @IsOptional()
  public salesAccount: string;

  @ApiProperty()
  @IsOptional()
  public purchanseAccount: string;

  @ApiProperty()
  @IsOptional()
  readonly paymentCollection: string;

}