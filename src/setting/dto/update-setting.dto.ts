import { IsEnum, ValidateNested, IsOptional } from 'class-validator';
import { Type } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';
import { UserLanguageEnumAsArray, UserLanguageEnum } from '../../user/enums/user-language.enum';
import { Theme } from './theme-dto';
import { Account } from './account-dto';
import { SalesAndPurchanse } from './sales-and-purchanse-dto';
import { Preference } from './preference-dto';

export class UpdateSettingDto {

  @ApiProperty({ enum: UserLanguageEnumAsArray })
  @IsEnum(UserLanguageEnum, { each: true })
  readonly language: UserLanguageEnum;

  @ValidateNested({ each: true })
  @Type(() => Account)
  readonly account: Account;

  @ValidateNested({ each: true })
  @Type(() => Theme)
  readonly theme: Theme;

  @ValidateNested({ each: true })
  @Type(() => Preference)
  readonly preference: Preference;

  @ValidateNested({ each: true })
  @Type(() => SalesAndPurchanse)
  readonly salesAndPurchases: SalesAndPurchanse;
}
