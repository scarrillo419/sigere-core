import { IsString, IsOptional, IsEnum, ValidateNested } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { CurrencyEnum, CurrencyEnumAsArray } from '../../user/enums/user-currency.enum';
import { DecimalsEnumAsArray, DecimalsEnum } from '../../user/enums/user-decimals.enum';
import { DocumentsEnumAsArray, DocumentsEnum } from '../../user/enums/user-documents.enum';
import { NumericalFormatEnumAsArray, NumericalFormatEnum } from '../../user/enums/user-numericalFormat.enum';
import { DateFormatEnum, DateFormatEnumAsArray } from '../../user/enums/user-dateFormat.enun';
import { UserLanguageEnumAsArray, UserLanguageEnum } from '../../user/enums/user-language.enum';
import { PdfDefaultDto } from './pdf-default-dto';
import { Type } from 'class-transformer';

export class Preference {
  @ApiProperty({ enum: DecimalsEnumAsArray })
  @IsEnum(DecimalsEnum, { each: true })
  @IsOptional()
  readonly decimals: DecimalsEnum;

  @ApiProperty({ enum: CurrencyEnumAsArray })
  @IsEnum(CurrencyEnum, { each: true })
  @IsOptional()
  readonly currency: CurrencyEnum ;
  
  @ApiProperty({ enum: UserLanguageEnumAsArray })
  @IsEnum(UserLanguageEnum, { each: true })
  @IsOptional()
  readonly languageDocuments: UserLanguageEnum;
 
  @ApiProperty({ enum: DocumentsEnumAsArray })
  @IsEnum(DocumentsEnum, { each: true })
  @IsOptional()
  readonly documents: DocumentsEnum;

  @ApiProperty({ enum: NumericalFormatEnumAsArray })
  @IsEnum(NumericalFormatEnum, { each: true })
  @IsOptional()
  readonly numericalFormat: NumericalFormatEnum;

  @ApiProperty({ enum: DateFormatEnumAsArray})
  @IsEnum(DateFormatEnum, { each: true })
  @IsOptional()
  readonly dateFormat: DateFormatEnum;

  @ApiProperty()
  @IsOptional()
  @IsString()
  readonly timeZone : string;

  @ApiProperty()
  @IsOptional()
  @ValidateNested({ each: true })
  @Type(() => PdfDefaultDto)
  readonly themePdfDefaul: PdfDefaultDto[];
}