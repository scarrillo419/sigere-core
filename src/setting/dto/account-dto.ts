import { IsString, IsOptional, IsEnum, IsEmail } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class Account {
  @ApiProperty()
  @IsString()
  @IsOptional()
  readonly nameCorpor: string;

  @ApiProperty()
  @IsOptional()
  @IsString()
  readonly numIdentif: string;

  @ApiProperty()
  // @IsEmail()
  @IsOptional()
  readonly email: string;

  @ApiProperty()
  @IsString()
  @IsOptional()
  readonly phone: number;

  @ApiProperty()
  @IsString()
  @IsOptional()
  public address: string;

  @ApiProperty()
  @IsOptional()
  @IsString()
  readonly population: string;

  @ApiProperty()
  @IsOptional()
  @IsString()
  readonly zipCode: number;

  @ApiProperty()
  @IsOptional()
  @IsString()
  readonly province: string;

}