import { IsString, IsOptional, IsEnum } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { CrooperDto } from './cropper-dto';
import { Type } from 'class-transformer';

export class Theme {

  @ApiProperty()
  @IsOptional()
  @IsString()
  readonly primaryColor: string;

  @ApiProperty()
  @IsOptional()
  @IsString()
  readonly secundaryColor: string;

  @ApiProperty()
  @IsOptional()
  @IsString()
  readonly textColor: string;

  @ApiProperty()
  @IsOptional()
  @IsString()
  readonly textColorSecundary: string;

  @ApiProperty()
  @IsOptional()
  @IsString()
  readonly headerColor: string;

  @ApiProperty()
  @IsOptional()
  @IsString()
  readonly sideBarColor: string;

  @ApiProperty()
  @IsOptional()
  public photoCorporative : string;

  @ApiProperty()
  @Type(() => CrooperDto)
  @IsOptional()
  readonly crooperData: CrooperDto;

  @ApiProperty()
  @IsOptional()  
  readonly file: any;

}
