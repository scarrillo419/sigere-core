import { Body, Controller, Put,
  UseFilters, UseGuards, UploadedFile, Req, UseInterceptors  } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { ApiTags, ApiBearerAuth } from '@nestjs/swagger';
import { TokenRequirements } from '../common/decorators/token-requirements.decorator';
import { HttpExceptionFilter } from '../common/filters/http-exception.filter';
import { MongoExceptionFilter } from '../common/filters/mongo-exception.filter';
import { TokenTypeEnum } from '../token/enums/token-type.enum';
import { TokenGuard } from '../common/guards/token.guard';
import { AccessToken } from '../token/interfaces/access-token.interface';
import { Token } from '../common/decorators/token.decorator';
import { diskStorage } from 'multer';
import { extname } from 'path';
import { Setting } from './interfaces/setting.interface';
import { SettingService } from './setting.service';
import { UpdateSettingDto } from './dto/update-setting.dto';
import { ModulesKeyEnum } from '../user/enums/modules-key.enum';
import { RoleOptionsEnum } from '../user/enums/role-options.enum';

@ApiTags('Setting')
@ApiBearerAuth()
@Controller('api/setting')
@UseFilters(MongoExceptionFilter, HttpExceptionFilter)
@UseGuards(TokenGuard)
export class SettingController {
  constructor(
    private readonly SettingService: SettingService,
    ) { }

  /**
   * Update the own user
   * @param {UpdateUserSelfDto} updateUserSelfDto - A data-transfer-object describing the user to be updated
   * @return {User} The updated user
   */
  @Put()
  @UseInterceptors(FileInterceptor('theme[file]', {
    storage: diskStorage({
      destination: './public/users/',
      filename: (req, file, cb) => {
        // Generating a 32 random chars long string
        const randomName = Array(32).fill(null).map(() => (Math.round(Math.random() * 16)).toString(16)).join('');
        // Calling the callback passing the random name generated with the original extension name
        cb(null, `${randomName}${extname(file.originalname)}`);
      },
      limits: { fileSize: 2 * 1024 * 1024 },
    }),
  }))
  @TokenRequirements([TokenTypeEnum.system, TokenTypeEnum.client])
  public async updateSettingsSelf(
    @Token() token: AccessToken,
    @Body() updateSettingDto: UpdateSettingDto,
    @UploadedFile() file,
  ): Promise<Setting> {
    return this.SettingService.update(token, updateSettingDto, file);
  }

}
