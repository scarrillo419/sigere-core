import { Document } from 'mongoose';
import { UserLanguageEnum } from '../../user/enums/user-language.enum';
import { CurrencyEnum } from '../../user/enums/user-currency.enum';
import { DecimalsEnum } from '../../user/enums/user-decimals.enum';
import { DocumentsEnum } from '../../user/enums/user-documents.enum';
import { NumericalFormatEnum } from '../../user/enums/user-numericalFormat.enum';
import { DateFormatEnum } from '../../user/enums/user-dateFormat.enun';
import { ExpirationSettingsEnum } from '../../user/enums/user-expiration.enum';
export interface Account extends Document {
  nameCorpor: string;
  numIdentif: string;
  email: string;
  phone: number;
  address: string;
  population: number;
  zipCode: number;
  province: string;
  country: string;
}

export interface Theme extends Document {
  readonly navTheme: string;
  readonly primaryColor: string;
  readonly secundaryColor: string;
  readonly textColor: string;
  readonly textSecundaryColor: string;
  readonly sideBarColor: string;
  readonly headerColor: string;
  readonly photoCorporative: string;
}

export interface SalesAndPurchanse extends Document {
  readonly expiration: ExpirationSettingsEnum;
  readonly saleTax: string;
  readonly purchaseTax: string;
  salesAccount: string;
  puchanseAccount: string;
  readonly paymentCollection: string;
}
export interface Preference extends Document {
  readonly currency: CurrencyEnum;
  readonly decimals: DecimalsEnum;
  readonly languageDocuments: UserLanguageEnum;
  readonly documents: DocumentsEnum;
  readonly numericalFormat: NumericalFormatEnum;
  readonly dateFormat: DateFormatEnum;
  readonly timeZone: string;
}

export interface Setting extends Document {
  readonly theme: Theme;
  readonly account: Account;
  readonly preference: Preference;
  readonly salesAndPurchases: SalesAndPurchanse;
}
