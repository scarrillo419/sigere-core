import { forwardRef, BadRequestException, Injectable, Inject, NotFoundException } from '@nestjs/common';
import { Types as MongooseTypes, PaginateModel, Model } from 'mongoose';
import { Setting } from './interfaces/setting.interface';
import { UpdateSettingDto } from './dto/update-setting.dto';
import { isEmpty } from 'lodash';
import { unlinkSync } from 'fs';
import * as gm from 'gm';
import { publicPath } from '../common/constants/path.constants';
import { TaxService } from '../tax/tax.service';
import { UserService } from '../user/user.service';

@Injectable()
export class SettingService {
  constructor(@Inject('SettingModelToken') public readonly settingModel: PaginateModel<Setting>,
    @Inject(forwardRef(() => TaxService)) public readonly taxService: TaxService,
    @Inject(forwardRef(() => UserService)) public readonly userService: UserService,
  ) { }

  /**
   * Get a single Setting through defined criterias
   * @param {object} criteria - The defined criterias
   * @param {string} projection - Optional projection field (i.e. which fields to return)
   * @return {Setting} A single Setting
   */
  public async findOne(criteria: object, projection?: string): Promise<Model<Setting>> {
    if (!criteria) { throw new BadRequestException(); }
    const Setting = await this.settingModel.findOne(criteria, projection).exec();
    if (!Setting) { throw new NotFoundException(); }
    return Setting;
  }

  /**
 * Get a single Setting through defined criterias
 * @param {object} criteria - The defined criterias
 * @param {string} projection - Optional projection field (i.e. which fields to return)
 * @return {Setting} A single Setting
 */
  public async findOneDetail(userid: MongooseTypes.ObjectId): Promise<Model<Setting>> {
    const setting = await this.settingModel.findOne({})
      .populate([
        {
          path: 'salesAndPurchases.saleTax',
          populate: { path: 'taxes' }
        },
        {
          path: 'salesAndPurchases.purchaseTax',
          populate: { path: 'taxes' }
        },
        {
          path: 'salesAndPurchases.salesAccount',
          populate: { path: 'accountingAccount' }
        },
      ]).exec();

    if (!setting) { throw new NotFoundException(); }
    return setting;
  }

  /**
   * Get a single Setting through its identifier
   * @param {ObjectId} Settingid - The mongodb object id
   * @return {Setting} A single Setting
   */
  public async findOneById(settingid: MongooseTypes.ObjectId): Promise<Model<Setting>> {
    return await this.settingModel.findOne({ _id: settingid })
      .populate([
        {
          path: 'salesAndPurchases.saleTax',
          populate: { path: 'taxes' }
        },
        {
          path: 'salesAndPurchases.purchaseTax',
          populate: { path: 'taxes' }
        },
        {
          path: 'salesAndPurchases.salesAccount',
          populate: { path: 'accountingAccounts' }
        },
      ]).exec();
  }

  /**
   * Update an existing Setting
   * @param {ObjectId} Settingid - The mongodb object id
   * @param {UpdateSettingDto} updateSettingDto - A data-transfer-object describing the Setting to be updated
   * @return {Setting} The updated Setting
   */
  public async update(token, updateSettingDto: UpdateSettingDto, file: any): Promise<Model<Setting>> {
    const dbSettings = await this.findOne({});
    if (!isEmpty(file) && dbSettings) {
      if (dbSettings.photoCorporative) {
        try {
          await unlinkSync(`${publicPath.users}/${dbSettings.photoCorporative}`);
        } catch (e) {
          console.log('file no exist');
        }
      }

      await new Promise((resolve, reject) => {
        gm(`${publicPath.users}/${file.filename}`)
          .crop(
            updateSettingDto.theme.crooperData.width,
            updateSettingDto.theme.crooperData.height,
            updateSettingDto.theme.crooperData.x,
            updateSettingDto.theme.crooperData.y)
          .write(`${publicPath.users}/${file.filename}`, (err) => {
            if (err) {
              reject(err);
            } else resolve();
          });
      });
      updateSettingDto.theme.photoCorporative = file.filename;
    }

    if (updateSettingDto.salesAndPurchases) {

      // Cast of Taxes
      const saleTax = updateSettingDto.salesAndPurchases.saleTax;
      const purchaseTax = updateSettingDto.salesAndPurchases.purchaseTax;

      const dbSaleTax = await this.taxService.taxModel.findOne({ _id: saleTax });
      const dbPurchanseTax = await this.taxService.taxModel.findOne({ _id: purchaseTax });

      const findTax = dbSaleTax;
      const findPurchanseTax = dbPurchanseTax;
      // Validation in the db of taxes
      if (saleTax && findTax) {
        if (dbSaleTax && dbSaleTax._id.toString() === saleTax.toString()) {
          updateSettingDto.salesAndPurchases.saleTax = dbSaleTax._id;
        }
      }
      if (purchaseTax && findPurchanseTax) {
        if (dbPurchanseTax && dbPurchanseTax._id.toString() === purchaseTax.toString()) {
          updateSettingDto.salesAndPurchases.purchaseTax = dbPurchanseTax._id;
        }
      }
      if (!findTax) delete updateSettingDto.salesAndPurchases.saleTax;
      if (!findPurchanseTax) delete updateSettingDto.salesAndPurchases.purchaseTax;

      // Cast of accounts
      // const salesAccount = updateSettingDto.salesAndPurchases.salesAccount;

      // const dbSaleAccount = await this.accountingAccountService.accountingAccountModel.find({ _id: { $in: salesAccount } });
      // // Validation in the db of AccountingAccount
      // if (updateSettingDto.salesAndPurchases.salesAccount) {
      //   dbSaleAccount.filter((doc) => {
      //     if (doc._id.toString() === updateSettingDto.salesAndPurchases.salesAccount.toString()) {
      //       updateSettingDto.salesAndPurchases.salesAccount = doc._id;
      //     }
      //   });
      // }
    }
    dbSettings.set(updateSettingDto);
    await dbSettings.save();
    return await this.findOneById(dbSettings._id);
  }

}
