import { Connection } from 'mongoose';
import * as mongoosePaginate from 'mongoose-paginate';
import { SettingSchema } from './schemas/setting.schema';

SettingSchema.plugin(mongoosePaginate);

export const settingProviders = [
  {
    provide: 'SettingModelToken',
    useFactory: (connection: Connection) => connection.model('Setting', SettingSchema),
    inject: ['DbConnectionToken'],
  },
];
