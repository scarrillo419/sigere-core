import { Module, forwardRef } from '@nestjs/common';
import { DatabaseModule } from '../common/database/database.module';
import { AuthModule } from '../auth/auth.module';
import { SettingController } from './setting.controller';
import { settingProviders } from './setting.providers';
import { SettingService } from './setting.service';
import { TaxModule } from '../tax/tax.module';
import { UserModule } from '../user/user.module';

@Module({
  imports: [DatabaseModule,
     forwardRef(() => AuthModule) ,
     forwardRef(() => TaxModule),
     forwardRef(() => UserModule)],
  controllers: [SettingController],
  providers: [SettingService, ...settingProviders],
  exports: [SettingService]
})
export class SettingModule { }
