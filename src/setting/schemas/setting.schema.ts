import { Schema } from 'mongoose';
import { UserLanguageEnumAsArray } from '../../user/enums/user-language.enum';
import { CurrencyEnumAsArray } from '../../user/enums/user-currency.enum';
import { DecimalsEnumAsArray } from '../../user/enums/user-decimals.enum';
import { DocumentsEnumAsArray } from '../../user/enums/user-documents.enum';
import { NumericalFormatEnumAsArray } from '../../user/enums/user-numericalFormat.enum';
import { DateFormatEnumAsArray } from '../../user/enums/user-dateFormat.enun';
import { ExpirationSettingsEnumAsArray } from '../../user/enums/user-expiration.enum';

const SalesAndPurchanse = new Schema({
  expiration: { type: String, enum: ExpirationSettingsEnumAsArray },
  saleTax: { type: Schema.Types.ObjectId, ref: 'Tax' },
  purchaseTax: { type: Schema.Types.ObjectId, ref: 'Tax' },
  salesAccount: { type: Schema.Types.ObjectId, ref: 'AccountingAccount' },
  paymentCollection: { type: String },
});

const ThemeSchema = new Schema({
  navTheme: { type: String },
  primaryColor: { type: String },
  secundaryColor: { type: String },
  textColor: { type: String },
  textSecundaryColor: { type: String },
  sideBarColor: { type: String },
  headerColor: { type: String },
  photoCorporative: { type: String },
}, { _id: false });

const Account = new Schema({
  nameCorpor: { type: String },
  numIdentif: { type: String },
  email: { type: String },
  phone: { type: Number },
  address: { type: String },
  population: { type: String },
  zipCode: { type: Number },
  province: { type: String },
  country: { type: String },
}, { _id: false });

const DefaultTheme = new Schema({
  module: { type: String },
  theme: { type: String },
}, { _id: false });

const Preference = new Schema({
  currency: { type: String, enum: CurrencyEnumAsArray },
  decimals: { type: String, enum: DecimalsEnumAsArray },
  languageDocuments: { type: String, enum: UserLanguageEnumAsArray },
  documents: { type: String, enum: DocumentsEnumAsArray },
  numericalFormat: { type: String, enum: NumericalFormatEnumAsArray },
  dateFormat: { type: String, enum: DateFormatEnumAsArray },
  timeZone: { type: String },
  themePdfDefaul: [{ type: DefaultTheme }],
}, { _id: false });

export const SettingSchema = new Schema({
  theme: { type: ThemeSchema, default: {} },
  account: { type: Account, default: {} },
  preference: { type: Preference, default: {} },
  // salesAndPurchases: { type: SalesAndPurchanse, default: {} },
}, {
  timestamps: { createdAt: 'createdAt', updatedAt: 'updatedAt' },
  // do not return certain fields when saving the document
  toObject: {
    transform: (doc, ret, options) => {
      // delete ret.password;
      return ret;
    },
  },
  toJSON: {
    transform: (doc, ret, options) => {
      delete ret.__v;
      return ret;
    },
  },
});