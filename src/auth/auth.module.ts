import { forwardRef, Module } from '@nestjs/common';
import { UserModule } from '../user/user.module';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { refreshTokenProviders } from './refreshtoken.providers';
import { DatabaseModule } from '../common/database/database.module';
import { SettingModule } from '../setting/setting.module';

@Module({
  imports: [DatabaseModule, 
    forwardRef(() => UserModule),
    forwardRef(() => SettingModule)],
  controllers: [AuthController],
  providers: [AuthService, ...refreshTokenProviders],
  exports: [AuthService],
})
export class AuthModule { }
