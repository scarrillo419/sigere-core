import { IsString, MinLength, IsNotEmpty, Validate } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { PasswordsNotMatch } from '../../common/validators/equalsTo';

export class ResetPasswordUpDto {
 
  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  @MinLength(8, {
    message: 'register.error.passwordToShort'
  })
  readonly password: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  @MinLength(8, {
    message: 'register.error.passwordToShort'
  })
  @Validate(PasswordsNotMatch, {
    message: 'register.error.passwordsNotMatch'
  })
  readonly confirmPassword: string;
}
