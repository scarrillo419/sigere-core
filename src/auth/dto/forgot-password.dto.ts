import { IsString, IsEmail, IsNotEmpty, Validate } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class ForgotPasswordUpDto {
  @ApiProperty()
  @IsEmail({ }, { message: 'register.error.invalidEmail' })
  @IsNotEmpty()
  @IsString() readonly email: string;
}
