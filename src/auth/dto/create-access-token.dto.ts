import { IsEmail, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreateAccessTokenDto {
  @ApiProperty()
  @IsEmail() readonly email: string;

  @ApiProperty()
  @IsString() readonly refresh_token: string;
}
