import { IsString, IsEmail, MinLength, IsNotEmpty, Equals, Validate } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { NoExist } from '../../common/database/database.validator';
import { PasswordsNotMatch } from '../../common/validators/equalsTo';

export class SignUpDto {

  @ApiProperty()
  @IsEmail({ }, { message: 'register.error.invalidEmail' })
  @IsNotEmpty()
  @Validate(NoExist, ['User'], {
    message: 'register.error.userExists'
  })
  readonly email: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  @MinLength(8, {
    message: 'register.error.passwordToShort'
  })
  readonly password: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  @MinLength(8, {
    message: 'register.error.passwordToShort'
  })
  @Validate(PasswordsNotMatch, {
    message: 'register.error.passwordsNotMatch'
  })
  readonly confirmPassword: string;
}
