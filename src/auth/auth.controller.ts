import { Body, Controller, Post, UseFilters, Res, UseGuards, Req } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { HttpExceptionFilter } from '../common/filters/http-exception.filter';
import { MongoExceptionFilter } from '../common/filters/mongo-exception.filter';
import { CreateAccessTokenDto } from './dto/create-access-token.dto';
import { SignInDto } from './dto/sign-in.dto';
import { SignOutDto } from './dto/sign-out.dto';
import { SignUpDto } from './dto/sign-up.dto';
import { ForgotPasswordUpDto } from './dto/forgot-password.dto';
import { ResetPasswordUpDto } from './dto/reset-password.dto';
import { SignInReturnValue } from './interfaces/signin-returnvalue.interface';
import { SignOutReturnValue } from './interfaces/signout-returnvalue.interface';
import { CreateAccessTokenReturnValue } from './interfaces/create-accesstoken-returnvalue.interface';
import { AuthService } from './auth.service';
import { User } from '../user/interfaces/user.interface';
import { TokenGuard } from '../common/guards/token.guard';
import { AccessToken } from '../token/interfaces/access-token.interface';
import { Token } from '../common/decorators/token.decorator';
import { TokenRequirements } from '../common/decorators/token-requirements.decorator';
import { TokenTypeEnum } from '../token/enums/token-type.enum';
import { ModulesKeyEnum } from '../user/enums/modules-key.enum';
import { RoleOptionsEnum } from '../user/enums/role-options.enum';

@ApiTags('Auth')
@Controller('api/auth')
@UseFilters(MongoExceptionFilter, HttpExceptionFilter)
@UseGuards(TokenGuard)
export class AuthController {
  constructor(private readonly authService: AuthService) { }

  @Post('signin')
  async signIn(@Body() signInDto: SignInDto): Promise<SignInReturnValue> {
    return await this.authService.signIn(signInDto);
  }

  @Post('signup')
  async signUp(@Body() signUpDto: SignUpDto, @Req() request): Promise<User> {
    return await this.authService.signUp(signUpDto, request);
  }

  @Post('signout')
  async signOut(@Body() signOutDto: SignOutDto): Promise<SignOutReturnValue> {
    return await this.authService.signOut(signOutDto);
  }

  @Post('accesstoken')
  async createUserAccessToken(@Body() createAccessTokenDto: CreateAccessTokenDto): Promise<CreateAccessTokenReturnValue> {
    return await this.authService.createAccessTokenFromRefreshToken(createAccessTokenDto);
  }

  @Post('forgot-password')
  async forgotPassword(@Body() forgotPasswordUpDto: ForgotPasswordUpDto) {
    return await this.authService.forgotPassword(forgotPasswordUpDto);
  }

  @Post('reset-password')
  @TokenRequirements([TokenTypeEnum.system, TokenTypeEnum.client],
  [ModulesKeyEnum.user],
  [RoleOptionsEnum.edit])
  async resetPassword(@Token() token: AccessToken, @Body() resetPasswordUpDto: ResetPasswordUpDto): Promise<User> {
    return await this.authService.resetPassword(token, resetPasswordUpDto);
  }

  @Post('verify-user')
  @TokenRequirements([TokenTypeEnum.system, TokenTypeEnum.email])
  async verifyUser(@Token() token: AccessToken): Promise<User> {
    return await this.authService.verifyUser(token);
  }

  @Post('refresh-token')
  @TokenRequirements([TokenTypeEnum.system, TokenTypeEnum.email])
  async refreshAccessToken(@Token() token: AccessToken) {
    return await this.authService.refreshAccessToken(token);
  }

}
