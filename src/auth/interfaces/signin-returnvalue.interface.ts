import { User } from '../../user/interfaces/user.interface';
import { Setting } from '../../setting/interfaces/setting.interface';

export interface SignInReturnValue {
  readonly refresh_token: string;
  readonly access_token: string;
  readonly user: User;
  readonly settings: Setting;
}
