import { forwardRef, Inject, Injectable, UnauthorizedException, BadRequestException, NotFoundException } from '@nestjs/common';
import * as jwt from 'jsonwebtoken';
import { Model } from 'mongoose';
import { generate as generateRandString } from 'randomstring';
import { generate } from 'generate-password';
import { AuthConstants } from '../common/constants/auth.constants';
import { TokenTypeEnum } from '../token/enums/token-type.enum';
import { AccessToken } from '../token/interfaces/access-token.interface';
import { RefreshToken } from '../token/interfaces/refresh-token.interface';
import { User } from '../user/interfaces/user.interface';
import { SignInDto } from './dto/sign-in.dto';
import { ForgotPasswordUpDto } from './dto/forgot-password.dto';
import { ResetPasswordUpDto } from './dto/reset-password.dto';
import { SignInReturnValue } from './interfaces/signin-returnvalue.interface';
import { UserService } from '../user/user.service';
import { SignOutDto } from './dto/sign-out.dto';
import { SignOutReturnValue } from './interfaces/signout-returnvalue.interface';
import { CreateAccessTokenReturnValue } from './interfaces/create-accesstoken-returnvalue.interface';
import { CreateAccessTokenDto } from './dto/create-access-token.dto';
import { SignUpDto } from './dto/sign-up.dto';
import { UserStatusEnum } from '../user/enums/user-status.enum';
import { newTokenMail } from '../common/mailjet/user';
import { SettingService } from '../setting/setting.service';

@Injectable()
export class AuthService {

  private jwtPrivateKey: String;
  private jwtPublicKey: String;

  constructor(
    @Inject(forwardRef(() => UserService)) public readonly userService: UserService,
    @Inject('RefreshTokenModelToken') private readonly refreshTokenModel: Model<RefreshToken>,
    @Inject(forwardRef(() => SettingService)) public readonly settingService: SettingService,
  ) {
    try {
      this.jwtPrivateKey = AuthConstants.cert.privateKey;
      this.jwtPublicKey = AuthConstants.cert.publicKey;
    } catch (err) {
      throw new Error('One or more certificates could not be loaded');
    }
  }

  public async signIn(signInDto: SignInDto): Promise<SignInReturnValue> {
    // get database user and validate password
    const dbUser = await this.userService.findOneByEmailWithPassword(signInDto.email.toLowerCase());

    if (!dbUser) throw new NotFoundException({ message: 'login.error.invalidEmail' });

    if (dbUser.status === UserStatusEnum.pendingEmail) { throw new BadRequestException({ message: 'Check your inbox for a verification email!' }); }
    if (dbUser.status === UserStatusEnum.inactive) { throw new UnauthorizedException({ message: 'login.error.inactiveUser' }); }
    const passwordsMatch = await this.userService.validateCredentials(dbUser, signInDto.password);
    if (!passwordsMatch) { throw new UnauthorizedException({ message: 'login.error.incorrectPassword' }); }

    // generate and store refresh token
    const refreshToken = generateRandString(AuthConstants.refresh_token.length);
    const userRefreshToken = new this.refreshTokenModel({
      refresh_token: refreshToken,
      email: dbUser.email,
    });
    await userRefreshToken.save();

    // generate an access token for immediate usage as well
    const accessToken = this.createAccessTokenFromUser(dbUser);
    const dbsetting = await this.settingService.settingModel
      .findOne({})
      .populate([
        {
          path: 'salesAndPurchases.saleTax',
          populate: { path: 'taxes' }
        },
        {
          path: 'salesAndPurchases.purchaseTax',
          populate: { path: 'taxes' }
        },
        {
          path: 'salesAndPurchases.salesAccount',
          populate: { path: 'accountingAccount' }
        },
      ]).exec();

    // create return object
    const returnValue: SignInReturnValue = {
      refresh_token: refreshToken,
      access_token: accessToken,
      user: {
        ...dbUser.toJSON(),
        type: TokenTypeEnum.client,
      },
      settings: dbsetting
    };
    return returnValue;
  }

  public async signUp(signUpDto: SignUpDto, req: Request): Promise<User> {
    // get database user and validate password
    const dbUser = await this.userService.signUp(signUpDto, req);

    // generate an access token for immediate usage as well
    const accessToken = this.createAccessTokenFromUserEmail(dbUser, TokenTypeEnum.email, '24h');

    // Send mail
    // newUserVerify(dbUser, accessToken);

    return dbUser;
  }

  public async signOut(signOutDto: SignOutDto): Promise<SignOutReturnValue> {
    const removedRefreshToken = await this.refreshTokenModel.findOneAndRemove({ refresh_token: signOutDto.refresh_token }).exec();

    const returnValue: SignOutReturnValue = {
      signedout: !!removedRefreshToken,
    };
    return returnValue;
  }

  public async createAccessTokenFromRefreshToken(createAccessTokenDto: CreateAccessTokenDto): Promise<CreateAccessTokenReturnValue> {
    const refreshToken = await this.refreshTokenModel.findOne({ refresh_token: createAccessTokenDto.refresh_token }).exec();
    if (refreshToken && refreshToken.email === createAccessTokenDto.email) {

      // extend lifetime of the user refresh token by saving it again
      await refreshToken.save();

      // create a new access token
      const dbUser = await this.userService.findOne({ email: createAccessTokenDto.email });
      const newUserAccessToken: CreateAccessTokenReturnValue = {
        access_token: this.createAccessTokenFromUser(dbUser),
      };
      return newUserAccessToken;
    }
    throw new UnauthorizedException();
  }

  private createAccessTokenFromUser(user: User): string {
    const roleModules = [];
    if (user.roles) {
      user.roles.map((menu: any) => {
        if (menu.modules) {
          menu.modules.map((item: any) => {
            roleModules.push(item);
          });
        }
      });
    }
    const payload: AccessToken = {
      uid: user._id,
      // type: isSystemUser ? TokenTypeEnum.system : TokenTypeEnum.client,
      type: TokenTypeEnum.system,
      sub: user.email,
      rs: roleModules,
    };
    return jwt.sign(payload, this.jwtPrivateKey, { expiresIn: '1y' });
  }

  private createAccessTokenFromUserEmail(user: User, tokenType, expiresIn): string {
    const payload: AccessToken = {
      uid: user._id,
      type: tokenType,
      sub: user.email,
      rs: user.roles,
    };
    const timeExpires = expiresIn;
    return jwt.sign(payload, this.jwtPrivateKey, { expiresIn: timeExpires });
  }

  public validateAccessToken(accessToken: string): AccessToken {
    return jwt.verify(accessToken, this.jwtPublicKey) as AccessToken;
  }

  public async refreshAccessToken(token: AccessToken): Promise<Model<User>> {
    const dbUser = await this.userService.findOneByEmail(token.sub);
    // generate an access token for immediate usage as well
    const newAccessToken = this.createAccessTokenFromUserEmail(dbUser, TokenTypeEnum.email, '24h');
    newTokenMail(dbUser, newAccessToken);
  }

  public async forgotPassword(forgotPasswordUpDto: ForgotPasswordUpDto): Promise<Model<User>> {
    const dbUser = await this.userService.findOneByEmailWithPassword(forgotPasswordUpDto.email.toLowerCase());
    const password = generate({
      length: 10,
      numbers: true
    });

    if (!dbUser) throw new NotFoundException({ message: 'forgot.error.userNotFound' });

    else {
      // generate an access token for immediate usage as well
      const accessToken = this.createAccessTokenFromUserEmail(dbUser, TokenTypeEnum.recoveryPassword, 300000);
      // forgotPasswordMail(dbUser, accessToken, password);

      // tslint:disable-next-line:object-literal-shorthand
      dbUser.set({ password: password });
      return dbUser.save();
    }
  }

  public async resetPassword(token: any, resetPasswordUpDto: ResetPasswordUpDto): Promise<Model<User>> {
    const dbUser = await this.userService.userModel.findOne({ email: token.sub })
      .populate([
        {
          path: 'roles',
        },
      ]).exec();

    if (!dbUser) throw new NotFoundException({ message: 'forgot.error.userNotFound' });
    else {
      if (dbUser.status === UserStatusEnum.pendingConfig) dbUser.set({ password: resetPasswordUpDto.password, status: UserStatusEnum.active });
      else { dbUser.set({ password: resetPasswordUpDto.password }); }
      dbUser.save();
      return dbUser;
    }
  }

  async verifyUser(token: AccessToken): Promise<Model<User>> {
    const dbUser = await this.userService.findOneByEmail(token.sub);
    if (dbUser.status === UserStatusEnum.pendingEmail) {
      dbUser.set({
        // status: UserStatusEnum.pendingConfig,
        status: UserStatusEnum.active,
      });
      return dbUser.save();
    }
    if (dbUser.status === UserStatusEnum.active) {
      throw new BadRequestException({ message: 'verify.error.alreadyVerifiedToken' });
    }
    throw new BadRequestException();
  }
}
