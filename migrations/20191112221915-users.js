const { Types } = require('mongoose');
const collection = 'users';
const env = require('../env');

const data = [
  {
    _id: Types.ObjectId(),
    roles: [Types.ObjectId(env.MASTER_ROLE_ID)],
    name: 'Admin',
    email: 'admin@admin.com',
    password: '2ed36cc0dc5c553af5e058fef80a66eef012d80259fc7cc2ea646d1522c752f197a6e3d24274c5d0c07a6ffb2f366d02yJQkY6AtcAEUaqPx8OD/Qg==',
    status: 'active'
  }
];

module.exports = {
  async up(db, client) {
    await db.collection(collection).insertMany(data);
  },

  async down(db, client) {
    await db.collection(collection).deleteMany({});
  }
};
