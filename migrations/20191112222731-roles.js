const {
  Types
} = require('mongoose');
const env = require('../env');

const collection = 'roles';

const data = [{
  _id: Types.ObjectId(env.MASTER_ROLE_ID),
  modules: [{
    permissions: ['view', 'create', 'edit', 'delete'],
    key: 'dashboard'
  }, {
    permissions: ['view', 'create', 'edit', 'delete'],
    key: 'user-admin'
  }, {
    permissions: ['view', 'create', 'edit', 'delete'],
    key: 'employee-admin'
  }, {
    permissions: [],
    key: 'company'
  }, {
    permissions: ['view', 'create', 'edit', 'delete'],
    key: 'team-admin'
  }, {
    permissions: ['view', 'create', 'edit', 'delete'],
    key: 'contact-admin'
  }, {
    permissions: ['view', 'create', 'edit', 'delete'],
    key: 'role-admin'
  }, {
    permissions: ['view', 'create', 'edit', 'delete'],
    key: 'budget-admin'
  }, {
    permissions: [],
    key: 'sales'
  }, {
    permissions: ['view', 'create', 'edit', 'delete'],
    key: 'sales-invoice-admin'
  }, {
    permissions: ['view', 'create', 'edit', 'delete'],
    key: 'sales-channel-admin'
  }, {
    permissions: ['view', 'create', 'edit', 'delete'],
    key: 'delivery-admin'
  }, {
    permissions: ['view', 'create', 'edit', 'delete'],
    key: 'expense'
  }, {
    permissions: [],
    key: 'purchases'
  }, {
    permissions: ['view', 'create', 'edit', 'delete'],
    key: 'purchase-order-admin'
  }, {
    permissions: ['view', 'create', 'edit', 'delete'],
    key: 'product-admin'
  }, {
    permissions: [],
    key: 'product'
  }, {
    permissions: ['view', 'create', 'edit', 'delete'],
    key: 'taxes-admin'
  }, {
    permissions: ['view', 'create', 'edit', 'delete'],
    key: 'sales-order-admin'
  }, {
    permissions: ['view', 'create', 'edit', 'delete'],
    key: 'formula-admin'
  }, {
    permissions: [],
    key: 'formula'
  }, {
    permissions: ['view', 'create', 'edit', 'delete'],
    key: 'variable-admin'
  }, {
    permissions: ['view', 'create', 'edit', 'delete'],
    key: 'way-to-pay-admin'
  }, {
    permissions: ['view', 'create', 'edit', 'delete'],
    key: 'funnel-admin'
  }, {
    permissions: ['view', 'create', 'edit', 'delete'],
    key: 'lead-admin'
  }],
  name: 'Super Admin',
  status: 'active',
}];

module.exports = {
  async up(db, client) {
    await db.collection(collection).insertMany(data);
  },

  async down(db, client) {
    await db.collection(collection).deleteMany({});
  }
};
