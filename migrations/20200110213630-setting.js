const { Types } = require('mongoose');
const collection = 'settings';

const data = {
  _id: Types.ObjectId(),
  theme: {},
  account: {},
  preference: {
      currency: 'eur',
      decimals: 'v1',
      languageDocuments: 'es',
      documents: 'pdf',
      numericalFormat: 'v2',
      dateFormat: 'd1'
  },
  salesAndPurchases: {
      expiration: 'withoutcaducity'
  },
};

module.exports = {
  async up(db, client) {
    await db.collection(collection).insert(data);
  },

  async down(db, client) {
    await db.collection(collection).deleteMany({});
  }
};
