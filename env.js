// In this file you can configure migrate-mongo
const dotenv = require('dotenv');
const fs = require('fs');
const env = dotenv.parse(fs.readFileSync('.env'));

module.exports = env;